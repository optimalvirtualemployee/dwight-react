import { userConstants } from '../constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function registration(state = initialState, action) {
  switch (action.type) {
    case userConstants.REGISTER_REQUEST:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.REGISTER_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.REGISTER_FAILURE:
      if(user){
        return {
          loggedIn: true,
          user: action.user
        };
      }else{
        return {
          loggedIn: false
        };
      }
    default:
      return state
  }
}