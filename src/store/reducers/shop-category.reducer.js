import { shopConstants } from "store/constants/shop.constants";

export function shopCategory(state = {}, action) {
    switch (action.type) {
        case shopConstants.CATEGORY_REQUEST:
            return {
                loading: true,
            };
        case shopConstants.CATEGORY_SUCCESS:
            return {
                loading: false,
                categories: action.categories,
            };

        case shopConstants.CATEGORY_FAILURE:
            return {
                error: action.error,
            };
        default:
            return state;
    }
}
