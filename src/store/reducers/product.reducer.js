import { productConstants } from "../constants";

export function product(state = {}, action) {
  switch (action.type) {
    case productConstants.PRODUCT_LISTING_REQUEST:
      return {
        loading: true,
        items: action.products,
      };

      case productConstants.PRODUCT_LISTING_SUCCESS:
        console.log('in reducer',action)
      return {
        loading: true,
        items: action.products,
      };

      case productConstants.PRODUCT_LISTING_FAILURE:
      return {
        error: action.error,
      };

    case productConstants.GETALL_SUCCESS:
      return {
        items: action.users,
      };
    case productConstants.GETALL_FAILURE:
      return {
        error: action.error,
      };
    case productConstants.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map((user) =>
          user.id === action.id ? { ...user, deleting: true } : user
        ),
      };
    case productConstants.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        items: state.items.filter((user) => user.id !== action.id),
      };
    case productConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      return {
        ...state,
        items: state.items.map((user) => {
          if (user.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...userCopy } = user;
            // return copy of user with 'deleteError:[error]' property
            return { ...userCopy, deleteError: action.error };
          }

          return user;
        }),
      };

    case productConstants.CATEGORY_LISTING_REQUEST:
      
      return {
        loading: true,
        items: action.categories,
      };

    case productConstants.CATEGORY_LISTING_SUCCESS:
      return {
        items: action.categories,
      };
    case productConstants.CATEGORY_LISTING_FAILURE:
      return {
        error: action.error,
      };
    default:
      return state;
  }
}
