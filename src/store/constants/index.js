export * from './../constants/alert.constants';
export * from './../constants/user.constants';
export * from './../constants/home.constants';
export * from './../constants/product.constants';
export * from './../constants/shop.constants';
export * from './../constants/cart.constants';
export * from './../constants/promotions.constants';
export const GOOGLE_APP_ID =
  "625687168687-5nk6r6s7rjb08r7pgoivgg7nfrvmfn3m.apps.googleusercontent.com";
export const FACEBOOK_APP_ID = "780696132549771";