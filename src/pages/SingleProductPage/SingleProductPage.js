import React, { Component } from 'react';
import $ from 'jquery';
import './SingleProductPage.css';
// import Recommendation from 'components/recommendation/recommendation';
// import { Tabs, Tab } from "react-bootstrap";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();

class SingleProductPage extends Component {
   constructor(props) {
      super(props);
      document.title = "Dwight | Single Product Page";
      this.state = {
         key: "description_tab",
      };
      
      
      this.idURL = this.props.match.params.id; //Get ID from url by matching with the defined routes.
      this.pageData = this.props.location.state.data;
      this.renderSlider();
   }

   componentDidMount() {
      console.log(this.pageData);
      $('#lightSlider').lightSlider({
         gallery: true,
         item: 1,
         loop: true,
         slideMargin: 0,
         thumbItem: 9
      });
   }

   renderSlider = () => {
      let elements = [];
      if (this.pageData.logo) {
         for (var i = 0; i < 2; i++) {
            if (i === 0) {
               elements.push(
                  <li data-thumb={this.pageData.logo} className="clone left sliderImage">
                     <img src={this.pageData.logo} className="w-100" alt="img"/>
                  </li>
               );
            } else if (i === 5) {
               elements.push(
                  <li data-thumb={this.pageData.logo} className="clone right sliderImage">
                     <img src={this.pageData.logo} className="w-100" alt="img"/>
                  </li>
               );
            } else {
               elements.push(
                  <li data-thumb={this.pageData.logo} className="clone  sliderImage">
                     <img src={this.pageData.logo} className="w-100" alt="img" />
                  </li>
               );
            }
         }
      }
      this.elements = elements;
   };

   productPriceDetails = () => {
      if (this.pageData.productName) {
         return (
            <>
               <div className="about d-flex justify-content-between flex-column">
                  <h2 className="font-weight-bold d-flex align-items-center">$ {this.pageData.unitPrice}
                     {/* <strike className="ml-2 text-muted mr-3 actualPrice">$150</strike> */}
                  </h2>
                  <span className="text-muted"><span>{this.pageData.productName}</span></span>
               </div>
            </>
         );
      }
   }

   productDescriptions = () => {
      if (this.pageData.productName) {
         return (
            <>
               <div className="productDescription pl-3 mt-4">
                  <ul className="p-0">
                     <li>Stock <span className="inStock">{this.pageData.unitsInStock !== 0 && `In stock`}
                     {this.pageData.unitsInStock === 0 && `Out of stock`}</span></li>
                     {/* <li>Brand <a href="#">Real Time 7 </a></li> */}
                     {/* <li>Dimension <span>20x80x100</span></li> */}
                     {/* <li>Weight <span>1.00 KG</span></li> */}
                  </ul>
               </div>
               <div className="productLongDescritpion mt-3">
                  <h3>Description</h3>
                  <p>{this.pageData.productName}</p>
               </div>
            </>
         );
      }
   }
   addProductToCart=(e,item)=>{ 
      toast('Product Added to cart succesfully')  
    }

   render() {
      return (
         <section className="bg-light">
            <div className="container pt-4 pb-3 bg-white">
               <div className="row">
                  <nav aria-label="breadcrumb ">
                     <ol className="breadcrumb bg-transparent">
                        <li className="breadcrumb-item"><a href="/">Home</a></li>
                        <li className="breadcrumb-item"><a className="thisPageLink" href="/#">{this.pageData && this.pageData.category}</a></li>
                        <li className="breadcrumb-item active" aria-current="page">{this.pageData && this.pageData.productName}</li>
                     </ol>
                  </nav>
      
               </div>
               <div className="row no-gutters">
                  <div className="col-md-5 pr-2">
                     <div className="card">
                        <div className="demo">
                           <div className="lSSlideOuter ">
                              <div className="lSSlideWrapper usingCss">
                                 <ul id="lightSlider" className="lightSlider lsGrab lSSlide lsGrabElementEffc">
                                    {this.pageData && this.elements}
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="col-md-7">
                     <div className="p-4">
                        {this.productPriceDetails()}
                        <div className="productQtyCartBuy d-flex mt-3 row p-2  ">
                           <div className="col-lg-2 p-1">
                              <input type="number" min="1" className="from-control w-100 qty" defaultValue="1" placeholder="QTY" />
                           </div>
                           <div className="col-lg-7 p-1">
                              <button  onClick={(e) => this.addProductToCart(e)} className="btn btn-addToCart btn-long cart w-100"> <i className="fa fa-shopping-cart"></i> Add to cart</button>
                           </div>
                           <div className="col-lg-3 p-1">
                              <button className="btn btn-primary btn-long btnBuyNow w-100"> <i className="fa fa-dollar"></i> Buy Now</button>
                           </div>
                           <div></div>
                        </div>
                        {this.productDescriptions()}
                     </div>
                  </div>
               </div>
            </div>
         </section>
      );
   }
}

export default SingleProductPage;