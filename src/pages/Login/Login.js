import React from "react";
//Component import
import GoogleButton from "components/SocialButton/GoogleButton";
import FacebookButton from "components/SocialButton/FacebookButton";
//Redux configurations imports
import { connect } from "react-redux";
//Redux action imports
import { userActions } from "../../store/actions";

import {FACEBOOK_APP_ID,GOOGLE_APP_ID} from './../../store/constants/index';
class Login extends React.Component {
  constructor(props) {
    super(props);

    // reset login status
    // this.props.logout();
    this.state = {
      email: "",
      password: "",
      submitted: false,
    };
    this.nodes = {};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.handleSocialLogin = this.handleSocialLogin.bind(this);
    this.handleSocialLoginFailure = this.handleSocialLoginFailure.bind(this);
    this.onLogoutSuccess = this.handleLogoutSuccess.bind(this);
    this.onLogoutFailure = this.handleLogoutFailure.bind(this);
    this.logout = this.logout.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const { email, password } = this.state;
    if (email && password) {
      this.props.dispatch(userActions.login(email, password));
      
    }
  }

  handleSocialLogin = (user) => {
    let accessToken = user.token.accessToken;
    if(user._provider === "google"){
      this.props.dispatch(userActions.googleLogin(accessToken));
    }else{
      this.props.dispatch(userActions.facebookLogin(accessToken));
    }

    //
    // this.props.facebookLogin(accessToken);
  };

  handleSocialLoginFailure = (err) => {
    console.error(err);
  };

  triggerLogin = (e) => {
    console.log(e);
  };

  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node;
    }
  }

  // onLoginSuccess(user) {
  //   console.log(user);

  //   this.setState({
  //     logged: true,
  //     currentProvider: user._provider,
  //     user,
  //   });
  // }

  // onLoginFailure(err) {
  //   console.error(err);

  //   this.setState({
  //     logged: false,
  //     currentProvider: "",
  //     user: {},
  //   });
  // }

  handleLogoutSuccess() {
    this.setState({
      logged: false,
      currentProvider: "",
      user: {},
    });
  }

  handleLogoutFailure(err) {
    console.error(err);
  }

  logout() {
    const { logged, currentProvider } = this.state;

    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout();
    }
  }

  render() {
    const { loggingIn } = this.props;
    const { email, password, submitted } = this.state;
    return (
      <section>
        <div className="container">
          <div className="row mt-5 mb-5">
            <div className="col-lg-6 border p-4">
              <div className="inner contact-right">
                <h4 className="mb-3">Login Account!</h4>
                <form action="#" method="post" className="signin-form">
                  <div className="input-grids">
                    <div className="form-group">
                      <input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="Your Email*"
                        className="contact-input"
                        required=""
                        onChange={(e) => this.handleChange(e)}
                      />
                      {submitted && !email && (
                        <div className="help-block">Username is required</div>
                      )}
                    </div>
                  </div>
                  <div className="form-group d-flex">
                    <input
                      type="password"
                      name="password"
                      id="password"
                      placeholder="Password"
                      className="contact-input"
                      onChange={(e) => this.handleChange(e)}
                    />
                  </div>
                  {submitted && !password && (
                    <div className="help-block">Password is required</div>
                  )}
                  <div className="form-group d-flex align-items-center justify-content-between">
                    <label htmlFor="keepMeSignedIn" className="keepMeSignedIn">
                      <input
                        type="checkbox"
                        name="keepMeSignedIn"
                        id="keepMeSignedIn"
                        className="contact-input"
                        onChange={(e) => this.handleChange(e)}
                      />
                      Keep me Signed In
                    </label>
                    <a href="/registration">Register Account</a>
                  </div>
                  <div className="text-center">
                    <button
                      className="btn btn-primary theme-button"
                      onClick={this.handleSubmit}
                    >
                      Login Now
                    </button>
                    {loggingIn && (
                      <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" alt="img" />
                    )}
                  </div>
                  <hr />

                  <GoogleButton
                    provider="google"
                    appId={GOOGLE_APP_ID}
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                    onLogoutSuccess={this.handleLogoutSuccess}
                    onLogoutFailure={this.handleLogoutFailure}
                    getInstance={this.setNodeRef.bind(this, "google")}
                    key={"google"}
                  >
                    <i className="fa fa-google fa-fw"></i> Login with Google
                  </GoogleButton>

                  <FacebookButton
                    provider="facebook"
                    appId={FACEBOOK_APP_ID}
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                    onLogoutSuccess={this.handleLogoutSuccess}
                    getInstance={this.setNodeRef.bind(this, "facebook")}
                    key={"facebook"}
                    onInternetFailure={() => {
                      return true;
                    }}
                  >
                    <i className="fa fa-facebook fa-fw"></i> Login with Facebook
                  </FacebookButton>
                </form>
              </div>
            </div>
            <div className="col-lg-6 d-lg-block d-sm-none">
              <div className="inner text-center">
                <img
                  src="assets/images/forget-bg.png"
                  alt=""
                  className="w-75"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};


export default connect(mapStateToProps)(Login);
