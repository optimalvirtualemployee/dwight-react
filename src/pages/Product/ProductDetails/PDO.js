import React, { Component } from "react";
import $ from "jquery";
import "./ProductDetails.css";
import Recommendation from "components/recommendation/recommendation";
import { productActions } from "store/actions";
import { connect } from "react-redux";
import EnquiryForm from "components/forms/EnquiryForm/EnquiryForm";
import { NavLink } from "react-router-dom";
import SingleProductPage from "pages/SingleProductPage/SingleProductPage";
import InquiryModal from "./../Modal/InquiryModal";
import config from "services/config";
import { Modal, Tab, Tabs } from "react-bootstrap";
import AppContext from "contexts/AppContext";

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    document.title = "Dwight | Design Service";

    this.state = {
      show: false,
    };
    let pageData;
    let elements = [];
    this.idURL = this.props.match.params.id; //Get ID from url by matching with the defined routes.

    this.pageData = this.props.location.state.data;
    this.categoryId = this.props.location.state.categoryId;

    console.log("this.categoryId", this.categoryId);
    this.renderSlider();
  }

  componentDidMount() {
    $("#lightSlider").lightSlider({
      gallery: true,
      item: 1,
      loop: true,
      slideMargin: 0,
      thumbItem: 9,
    });
    $(".btn-addToCart").on("click", function () {
      var cart = $(".itemsCount");
      var imgtodrag = $(".lsGrabElementEffc").find("img").eq(0);
      console.log(imgtodrag);
      if (imgtodrag) {
        var imgclone = imgtodrag
          .clone()
          .offset({
            top: imgtodrag.offset().top,
            left: imgtodrag.offset().left,
          })
          .css({
            opacity: "0.5",
            position: "absolute",
            "max-height": "150px",
            "max-width": "150px",
            "z-index": "100",
          })
          .appendTo($("body"))
          .animate(
            {
              top: cart.offset().top + 10,
              left: cart.offset().left + 10,
              "max-width": "75px",
              "max-height": "75px",
            },
            1000,
            "easeInOutExpo"
          );

        setTimeout(function () {
          cart.effect(
            "shake",
            {
              times: 2,
            },
            200
          );
        }, 1500);

        imgclone.animate(
          {
            width: 0,
            "min-height": 0,
          },
          function () {
            $(this).detach();
          }
        );
        window.scrollTo(0, 0);
      }
    });
  }

  renderSlider = () => {
    let elements = [];
    if (this.pageData.logo) {
      elements.push(
        <li data-thumb={this.pageData.logo} className="clone left sliderImage">
          <img src={this.pageData.logo} className="w-100" alt="img" />
        </li>
      );

      if (this.pageData.images.length > 0) {
        for (var i = 0; i < this.pageData.images.length; i++) {
          if (i == 0) {
            elements.push(
              <li
                data-thumb={config.apiUrl + this.pageData.images[i].image}
                className="clone left sliderImage"
              >
                <img
                  src={config.apiUrl + this.pageData.images[i].image}
                  className="w-100"
                  alt="img"
                />
              </li>
            );
          } else if (i == 5) {
            elements.push(
              <li
                data-thumb={config.apiUrl + this.pageData.images[i].image}
                className="clone right sliderImage"
              >
                <img
                  src={config.apiUrl + this.pageData.images[i].image}
                  className="w-100"
                  alt="img"
                />
              </li>
            );
          } else {
            elements.push(
              <li
                data-thumb={config.apiUrl + this.pageData.images[i].image}
                className="clone  sliderImage"
              >
                <img
                  src={config.apiUrl + this.pageData.images[i].image}
                  className="w-100"
                  alt="img"
                />
              </li>
            );
          }
        }
      } else {
        for (var i = 0; i < 2; i++) {
          if (i == 0) {
            elements.push(
              <li
                data-thumb={this.pageData.logo}
                className="clone left sliderImage"
              >
                <img src={this.pageData.logo} className="w-100" alt="img" />
              </li>
            );
          } else if (i == 5) {
            elements.push(
              <li
                data-thumb={this.pageData.logo}
                className="clone right sliderImage"
              >
                <img src={this.pageData.logo} className="w-100" alt="img" />
              </li>
            );
          } else {
            elements.push(
              <li
                data-thumb={this.pageData.logo}
                className="clone  sliderImage"
              >
                <img src={this.pageData.logo} className="w-100" alt="img" />
              </li>
            );
          }
        }
      }
    }
    this.elements = elements;
  };

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  showModal = (e) => {
    this.setState({
      show: !this.state.show,
    });
  };

  productPriceDetails = () => {
    if (this.pageData.productName) {
      return (
        <>
          <div className="about d-flex justify-content-between flex-column">
            <h2 className="font-weight-bold d-flex align-items-center">
              $ {this.pageData.unitPrice}
              {/* <strike className="ml-2 text-muted mr-3 actualPrice">$150</strike> */}
            </h2>
            <span className="text-muted">
              <span>{this.pageData.productName}</span>
            </span>
          </div>
        </>
      );
    }
  };

  closeModal = () => {
    this.setState({ show: false })
  }

  productDescriptions = () => {
    if (this.pageData.productName) {
      return (
        <>
          <div className="productDescription pl-3 my-4">
            <ul className="p-0">
              <li>
                Stock
                <span className="inStock">
                  {this.pageData.unitsInStock != 0 && `In stock`}
                  {this.pageData.unitsInStock == 0 && `Out of stock`}
                </span>
              </li>
            </ul>
          </div>
          <Tabs defaultActiveKey="description" id="uncontrolled-tab-example">
            <Tab eventKey="description" title="Description">
              <div className="py-3">
                {this.pageData.productDescription}
              </div>
            </Tab>
            <Tab eventKey="videos" title="Videos">
              <div className="py-3">{" "}</div>
            </Tab>
          </Tabs>
        </>
      );
    }
  };

  render() {
    return (
      <AppContext.Consumer>
        {({ cartItems, setCartItems }) => (

          <section>
            <div className="container mt-4 mb-3">
              <div className="row">
                <nav aria-label="breadcrumb ">
                  <ol className="breadcrumb bg-transparent">
                    <li className="breadcrumb-item">
                      <NavLink to="/">Home</NavLink>
                    </li>
                    <li className="breadcrumb-item">
                      <NavLink
                        className="thisPageLink"
                        to={`/shop/${this.categoryId}`}
                      >
                        {this.pageData && this.pageData.category}
                      </NavLink>
                    </li>
                    <li className="breadcrumb-item active" aria-current="page">
                      Product Details
                </li>
                  </ol>
                </nav>
              </div>
              <div className="row no-gutters">
                <div className="col-md-5 pr-2">
                  <div className="card">
                    <div className="demo">
                      <div className="lSSlideOuter ">
                        <div className="lSSlideWrapper usingCss">
                          <ul
                            id="lightSlider"
                            className="lightSlider lsGrab lSSlide lsGrabElementEffc"
                          >
                            {/* {this.pageData && this.renderSlider} */}
                            {this.pageData && this.elements}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="card p-4">
                    <div className="about d-flex justify-content-between">
                      <span className="font-weight-bold">
                        {/* {this.productPriceDetails()} */}
                        {this.pageData && this.pageData.productName}
                      </span>
                      <span className="font-weight-bold">
                        ${this.pageData && this.pageData.unitPrice}
                      </span>
                    </div>
                    <hr />

                    <div className="product-description">
                      {this.productDescriptions()}

                      <div className="productQtyCartBuy d-flex mt-3 row p-2  ">
                        <div className="col-lg-2 p-1">
                          <input
                            type="number"
                            min="1"
                            className="from-control w-100 qty"
                            placeholder="QTY"
                            defaultValue="1"
                          />
                        </div>
                        <div className="col-lg-7 p-1">


                          <button
                            className="btn btn-addToCart btn-info cart w-100"
                            onClick={(e) => {
                              function insertIntoCart(
                                previousItems,
                                currentItem
                              ) {
                                cartItems = [
                                  ...previousItems,
                                  currentItem,
                                ];
                                return cartItems;
                              }
                              this.prevCartItems =
                                JSON.parse(
                                  localStorage.getItem("cartItems")
                                ) != null
                                  ? JSON.parse(
                                    localStorage.getItem(
                                      "cartItems"
                                    )
                                  )
                                  : [];
                              var currentCart = insertIntoCart(
                                this.prevCartItems,
                                this.pageData
                              );
                              // toast("Product Added to cart successfully");
                              localStorage.setItem(
                                "cartItems",
                                JSON.stringify(currentCart)
                              );
                              setCartItems(currentCart);
                            }}
                          >
                            <i className="fa fa-shopping" /> Add to
                                      Cart
                                    </button>
                        </div>
                        <div className="col-lg-3 p-1">
                          <button
                            className="btn btn-primary btn-long btnBuyNow w-100"
                            onClick={(e) => {
                              this.showModal(e);
                            }}
                          >
                            Inquiry
                      </button>
                        </div>
                      </div>
                      <Modal size="lg" show={this.state.show} onHide={this.showModal}>
                        <InquiryModal onClose={this.showModal} show={this.state.show}>
                          <div className="mt-4 mb-2">
                            {" "}
                            <span className="font-weight-bold">
                              Enter Inquiry Details
                             </span>
                          </div>
                          <EnquiryForm close={this.closeModal} />
                        </InquiryModal>
                      </Modal>
                    </div>
                  </div>
                </div>
              </div>
              {/* <Recommendation /> */}
            </div>
          </section>
        )}
      </AppContext.Consumer>
    );
  }
}

// export default ProductDetails;

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(ProductDetails); 2