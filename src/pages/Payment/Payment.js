import React, { Component } from 'react';
import './Payment.css';

class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        document.title = "Dwight | Payment";
    }

    render() {
        return (
            <section>
                <div className="payment-wrapper py-5">
                    <div className="container py-lg-3">
                        <div className="row justify-content-around">
                            <div className="col-md-6 mb-3 mb-md-0">
                                <div className="card">
                                    <div className="card-header pb-0">
                                        <h2 className="card-title space ">Checkout</h2>
                                        <p className="card-text text-muted mt-4 space">SHIPPING DETAILS</p>
                                    </div>
                                    <div className="card-body">
                                        <div className="row justify-content-between">
                                            <div className="col-auto mt-0">
                                                <p><b>Team Vasant Vihar 110020 New Delhi India</b></p>
                                            </div>
                                            <div className="col-auto">
                                                <p><b>Abc@gmail.com</b> </p>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col">
                                                <p className="text-muted mb-2">PAYMENT DETAILS</p>
                                            </div>
                                        </div>
                                        <form className="needs-validation signin-form" noValidate>
                                            <div className="form-group">
                                                <label>Name On Card</label>
                                                <input type="text" placeholder="Your Name*" className="form-control" required="" />
                                            </div>
                                            <div className="form-group">
                                                <label>Card Number</label>
                                                <input type="number" placeholder="***********" className="form-control" required="" />
                                            </div>
                                            <div className="row no-gutters">
                                                <div className="col-sm-6 pr-sm-2">
                                                    <div className="form-group">
                                                        <label>Valid Through</label>
                                                        <input type="text" placeholder="03/21" className="form-control" required="" />
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 pr-sm-2">
                                                    <div className="form-group">
                                                        <label>CVV Code</label>
                                                        <input type="number" placeholder="435" className="form-control" required="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group payment-btn">
                                                <button type="button" name="" class="btn btn-block btn-outline-primary btn-lg">Make Payment</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div class="card">
                                    <div className="card-header pb-0">
                                        <h2 className="card-title space ">Your Order</h2>
                                        <p className="card-text text-muted mt-4 space">EDIT SHOPPING</p>
                                    </div>
                                    <div className="card-body">
                                        <div className="row justify-content-between">
                                            <div className="col-auto col-md-7">
                                                <div className="media flex-column flex-sm-row">
                                                    <img className=" img-fluid" src="https://i.imgur.com/6oHix28.jpg" width="62" height="62" />
                                                    <div className="media-body my-auto">
                                                        <div className="row ">
                                                            <div className="col-auto">
                                                                <p className="mb-0"><b>EC-GO Bag Standard</b></p><small class="text-muted">1 Week Subscription</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className=" pl-0 flex-sm-col col-auto my-auto">
                                                <p className="boxed-1">2</p>
                                            </div>
                                            <div className=" pl-0 flex-sm-col col-auto my-auto ">
                                                <p><b>$179</b></p>
                                            </div>
                                        </div>
                                        <hr className="my-2" />
                                        <div className="row justify-content-between">
                                            <div className="col-auto col-md-7">
                                                <div className="media flex-column flex-sm-row">
                                                    <img className=" img-fluid" src="https://i.imgur.com/6oHix28.jpg" width="62" height="62" />
                                                    <div className="media-body my-auto">
                                                        <div className="row ">
                                                            <div className="col-auto">
                                                                <p className="mb-0"><b>EC-GO Bag Standard</b></p><small class="text-muted">1 Week Subscription</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className=" pl-0 flex-sm-col col-auto my-auto">
                                                <p className="boxed-1">2</p>
                                            </div>
                                            <div className=" pl-0 flex-sm-col col-auto my-auto ">
                                                <p><b>$179</b></p>
                                            </div>
                                        </div>
                                        <hr className="my-2" />
                                        <div className="row justify-content-between">
                                            <div className="col-auto col-md-7">
                                                <div className="media flex-column flex-sm-row">
                                                    <img className=" img-fluid" src="https://i.imgur.com/6oHix28.jpg" width="62" height="62" />
                                                    <div className="media-body my-auto">
                                                        <div className="row ">
                                                            <div className="col-auto">
                                                                <p className="mb-0"><b>EC-GO Bag Standard</b></p><small class="text-muted">1 Week Subscription</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className=" pl-0 flex-sm-col col-auto my-auto">
                                                <p className="boxed-1">2</p>
                                            </div>
                                            <div className=" pl-0 flex-sm-col col-auto my-auto ">
                                                <p><b>$179</b></p>
                                            </div>
                                        </div>
                                        <hr className="my-2" />
                                        <div className="row">
                                            <div className="col">
                                                <div className="row justify-content-between">
                                                    <div className="col-4">
                                                        <p className="mb-1"><b>Subtotal</b></p>
                                                    </div>
                                                    <div className="flex-sm-col col-auto">
                                                        <p className="mb-1"><b>$179</b></p>
                                                    </div>
                                                </div>
                                                <div className="row justify-content-between">
                                                    <div className="col">
                                                        <p className="mb-1"><b>Shipping</b></p>
                                                    </div>
                                                    <div className="flex-sm-col col-auto">
                                                        <p className="mb-1"><b>$0</b></p>
                                                    </div>
                                                </div>
                                                <div className="row justify-content-between">
                                                    <div className="col-4">
                                                        <p><b>Total</b></p>
                                                    </div>
                                                    <div className="flex-sm-col col-auto">
                                                        <p className="mb-1"><b>$537</b></p>
                                                    </div>
                                                </div>
                                                <hr className="my-0" />
                                            </div>
                                        </div>
                                        <div className="form-group payment-btn mt-4">
                                            <button type="button" name="" class="btn btn-block btn-outline-primary btn-lg">ADD GIFT CODE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Payment;