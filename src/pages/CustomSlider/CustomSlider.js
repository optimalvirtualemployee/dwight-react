import React, { Component } from 'react';
import $ from 'jquery';
import config from 'services/config';
class CustomSlider extends Component {
    constructor(props) {
        super(props)
        document.title = "Dwight | Custom Slider";
    }

    componentDidMount() {
        $('#lightSlider').lightSlider({
            gallery: true,
            item: 1,
            loop: true,
            slideMargin: 0,
            thumbItem: 9
        });
    }

    renderImages = () => {
        this.pageData = this.props.productData.items;
        let imageSrc;
        this.pageData.images.unshift({ product: this.pageData.name, image: this.pageData.logo })
        const slider = this.pageData.images.map((item, index) => {
            { imageSrc = null }
            { imageSrc = (item.image && item.image.startsWith("http")) ? item.image : config.apiUrl + item.image }

            if (item.image !== null) {
                return (<li
                    key={item.product + index}
                    data-thumb={imageSrc}>
                    <img src={imageSrc} className="w-100" alt="img" />
                </li>)
            }

            // return (
            //     // <li
            //     //     key={item.product + index}
            //     //     data-thumb={index === 0 ? item.image : config.apiUrl + item.image}
            //     //     className="clone sliderImage">
            //     //     <img src={index === 0 ? item.image : config.apiUrl + item.image}
            //     //         className="w-100" alt="img" />
            //     // </li>

        });

        return (
            <React.Fragment>
                { slider}
            </React.Fragment >
        )
    }

    render() {
        return (
            <div className="col-md-5 pr-2">
                <div className="card">
                    <div className="lSSlideOuter ">
                        <div className="lSSlideWrapper usingCss">
                            <ul id="lightSlider" className="lightSlider lsGrab lSSlide lsGrabElementEffc">
                                {this.renderImages()}
                                {/* <li data-thumb="https://i.imgur.com/KZpuufK.jpg" className="clone sliderImage"> <img src="https://i.imgur.com/KZpuufK.jpg" className="w-100" alt="img" /> </li> */}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default CustomSlider;