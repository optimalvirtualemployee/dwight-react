import React, { Component } from "react";
import "./Shop.css";
import * as $ from "jquery";
import "jquery-ui-bundle";
import {
  Nav,
  Navbar,
  NavDropdown /*, MenuItem,  Tabs, ButtonToolbar, Button, Table, ButtonGroup, Row, Col, Grid, Panel, FormGroup, FormControl*/,
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import { Modal } from "react-bootstrap";
import {
  productActions,
  ProductListPagination,
  shopActions,
} from "store/actions";
import CategoryAside from "components/CategoryAside/CategoryAside";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

let content = [];
var cartItems = [];
let pageNumberToCall = 1;

const productsPerPage = 4;
const currentPage = 1;
import { cartConstants } from "store/constants";
import AppContext from "contexts/AppContext";
import InquiryModal from "pages/Product/Modal/InquiryModal";
import EnquiryForm from "components/forms/EnquiryForm/EnquiryForm";
class Shop extends Component {
  constructor(props) {
    super(props);
    document.title = "Dwight | Shop";
    let shopProducts;
    this.pageNumbers = [];
    this.state = {
      show: false,
      currentProducts : [],
      shopProducts : this.props.shopProducts
    };

    // if (this.props.shopProducts) {
    //   if (
    //     this.props.shopProducts.shopProducts &&
    //     this.props.shopProducts.shopProducts.length > 0
    //   ) {
    //     this.PageName = this.props.shopProducts.shopProducts[0].category;
    //     if (this.pageNumberToCall !== undefined) {
    //       this.getCurrentProducts(this.pageNumberToCall);
    //       this.nowShowing = this.pageNumberToCall * productsPerPage;
    //     } else {
    //       this.getCurrentProducts(pageNumberToCall);
    //       this.nowShowing = pageNumberToCall * productsPerPage;
    //     }

    //     // Pagination needs to be handled via store
    //     this.totalCount = this.props.shopProducts.shopProducts.length;
    //     let totalPages = this.totalCount / 3;
    //     for (
    //       let i = 1;
    //       i <=
    //       Math.ceil(
    //         this.props.shopProducts.shopProducts.length / productsPerPage
    //       );
    //       i++
    //     ) {
    //       this.pageNumbers.push(i);
    //     }
    //   }
    // }
  }

  showModal = (e) => {
    this.setState({
      show: !this.state.show,
    });
  };

  componentDidMount() {
    productActions.getCategory();
    if (this.props.shopProducts) {
      if (
        this.props.shopProducts.shopProducts &&
        this.props.shopProducts.shopProducts.length > 0
      ) {
        this.PageName = this.props.shopProducts.shopProducts[0].category;
        if (this.pageNumberToCall !== undefined) {
          this.getCurrentProducts(this.pageNumberToCall);
          this.nowShowing = this.pageNumberToCall * productsPerPage;
        } else {
          this.getCurrentProducts(pageNumberToCall);
          this.nowShowing = pageNumberToCall * productsPerPage;
        }

        // Pagination needs to be handled via store
        this.totalCount = this.props.shopProducts.shopProducts.length;
        let totalPages = this.totalCount / 3;
        for (
          let i = 1;
          i <=
          Math.ceil(
            this.props.shopProducts.shopProducts.length / productsPerPage
          );
          i++
        ) {
          this.pageNumbers.push(i);
        }
        this.setState({shopProducts : this.props.shopProducts})
      }
    }

    $(".btn-addToCart").on("click", function () {
      var cart = $(".itemsCount");
      var imgtodrag = $(this).parent(".pBox").find("img").eq(0);
      if (imgtodrag) {
        var imgclone = imgtodrag
          .clone()
          .offset({
            top: imgtodrag.offset().top,
            left: imgtodrag.offset().left,
          })
          .css({
            opacity: "0.5",
            position: "absolute",
            "min-height": "150px",
            width: "150px",
            "z-index": "100",
          })
          .appendTo($("body"))
          .animate(
            {
              top: cart.offset().top + 10,
              left: cart.offset().left + 10,
              width: "75px",
              "min-height": "75px",
            },
            1000,
            "easeInOutExpo"
          );

        setTimeout(function () {
          cart.effect(
            "shake",
            {
              times: 2,
            },
            200
          );
        }, 1500);

        imgclone.animate(
          {
            width: 0,
            "min-height": 0,
          },
          function () {
            $(this).detach();
          }
        );
        window.scrollTo(0, 0);
      }
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      shopProducts: nextProps.shopProducts,
    };
   }

   componentDidUpdate(nextProps) {
    const { shopProducts } = this.props
    if (nextProps.shopProducts !== shopProducts) {
     if (shopProducts) {
      this.setState({shopProducts : shopProducts});
      this.getCurrentProducts(pageNumberToCall);
     }
    }
   }
  renderCategoryBanner() {
    if (this.props.shopProducts) {
      // if (this.props.shopProducts.shopProducts) {
      if (
        this.props.shopProducts.shopProducts &&
        this.props.shopProducts.shopProducts.length > 0
      ) {
        return (
          <div className="shopBanner">
            <img
              src={
                this.props.shopProducts &&
                this.props.shopProducts.shopProducts &&
                this.props.shopProducts.shopProducts[0].logo
              }
              className="w-100"
            />
            <h1>{this.props.shopProducts.shopProducts[0].category}</h1>
          </div>
        );
      } else {
      }
    }
  }

  viewSingleProduct(product) {
    this.props.history.push({
      pathname: "/product-details/" + product.id,
      state: { data: product, categoryId: this.props.shopProducts.categoryId },
    });
  }

  getCurrentProducts = (currentPage) => {
    if (this.state.shopProducts) {
      if (this.state.shopProducts.shopProducts) {
        const indexOfLastProduct = currentPage * productsPerPage;
        const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
        this.nowShowingFirstProductId = indexOfFirstProduct + 1;
        this.nowShowingLastProductId =
          indexOfLastProduct > this.totalCount
            ? this.totalCount
            : indexOfLastProduct;

        let products = this.state.shopProducts.shopProducts;
        this.currentProducts = products.slice(
          indexOfFirstProduct,
          indexOfLastProduct
        );

        this.setState({currentProducts:this.currentProducts})
      }
    }
  };

  viewProductDetail(product) {
    // this.props.dispatch()
    this.props.history.push({
      pathname: "/product-details/" + product.id,
      state: { data: product },
    });
  }

  renderTopBanner = () => {
    if (this.props.shopProducts) {
      if (
        this.props.shopProducts.shopProducts &&
        this.props.shopProducts.shopProducts.length > 0
      ) {
        return (
          <div className="col-md-12 p-0 h-50">
            <div className="shopBanner">
              <img
                src={this.props.shopProducts.shopProducts[0].logo}
                className="w-100"
              />
              <h1>{this.props.shopProducts.shopProducts[0].category}</h1>
            </div>
          </div>
        );
      }
    }
  };
  handleClick = (event) => {
    this.pageNumberToCall = event.target.id;
    this.props.dispatch(ProductListPagination(this.pageNumberToCall));
    this.getCurrentProducts(this.pageNumberToCall);
    // window.scrollTo({
    //   top: 0,
    //   behavior: "smooth",
    // });
  };


  compareValues = (key, order = 'asc') => {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }

      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  closeModal = () => {
    this.setState({ show: false })
  }

  sortByType = (e) => {
    if (e.target.value == 'DESC') {
      this.state.shopProducts.shopProducts.sort(this.compareValues('unitPrice', 'desc'));
      this.setState({currentProducts:this.state.shopProducts.shopProducts});
    }else{
      this.state.shopProducts.shopProducts.sort(this.compareValues('unitPrice'));
      this.setState({currentProducts:this.state.shopProducts.shopProducts});
    }
    this.getCurrentProducts(1);
  }
  render() {
    return (
      <AppContext.Consumer>
        {({ cartItems, setCartItems }) => (
          <section>
            {/* <div className="col-md-12 p-0 h-50">
              {this.props.shopProducts ? this.renderCategoryBanner() : ""}
            </div> */}
            <div className="row no-gutters">
              <nav aria-label="breadcrumb ">
                <ol className="breadcrumb bg-transparent">
                  <li className="breadcrumb-item">
                    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">
                    {this.PageName}
                  </li>
                </ol>
              </nav>
            </div>
            <div className="container-fluid mb-3">
              <div className="row no-gutters">
                <CategoryAside />
                <div className="col-md-9">
                  <div className="w-100">
                    <div className="d-flex align-items-center justify-content-between pb-4 results">
                      {this.state.shopProducts && (
                        <div>
                          Showing{" "}
                          {this.state.shopProducts &&
                            this.nowShowingFirstProductId}{" "}
                          -{" "}
                          {this.state.shopProducts &&
                            this.nowShowingLastProductId}{" "}
                          of {this.state.shopProducts && this.totalCount}
                        </div>
                      )}
                      <div>
                        <span>sort by price: </span>
                        <select
                          name="price"
                          id="price"
                          className="p-1 pl-2 pr-2"
                          onChange={(e) => { this.sortByType(e) }}
                        >
                          <option value="ASC" defaultValue>
                            Ascending
                          </option>
                          <option value="DESC">Descending</option>
                        </select>
                      </div>
                    </div>
                    <div className="inner p-5 bg-light">
                      <div className="row"></div>
                      {this.state.shopProducts &&
                        this.state.shopProducts.shopProducts &&
                        this.state.shopProducts.shopProducts.length > 0 ? (
                        <>
                          <div className="row">
                            {this.state.shopProducts &&
                              this.state.shopProducts.shopProducts &&
                              this.currentProducts &&
                              this.state.shopProducts.shopProducts.length > 0 &&
                              this.state.currentProducts.map((product, index) => (
                                <div className="col-xl-3 col-lg-4 col-md-6 offset-md-0 offset-sm-1 col-sm-10 offset-sm-1  my-2 mb-3" key={index}>
                                  <div className="card pBox">
                                    <Link
                                      to={`/product-details/${product.id}`}
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.viewSingleProduct(product);
                                      }}
                                    >
                                      <img
                                        className="card-img-top"
                                        src={product.logo}
                                      />
                                    </Link>
                                    <div className="card-body p-1">
                                      <div className="d-flex align-items-start justify-content-between border-bottom mb-1">
                                        <div className="d-flex flex-column">
                                          <div className="h6 font-weight-bold">
                                            <Link
                                              to={`/product-details/${product.id}`}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.viewSingleProduct(product);
                                              }}
                                              className="pNameEllipsis"
                                            >
                                              {product.name}
                                            </Link>
                                          </div>
                                          <div className="text-muted">
                                            $ {product.unitPrice}
                                          </div>
                                        </div>
                                        {/* <div className="btn pt-0">
                                          <span className="fa fa-heart" />
                                        </div> */}
                                      </div>
                                    </div>
                                    <div className="row"></div>
                                    <button
                                      className="btn btn-addToCart btn-info cart w-100"
                                      onClick={(e) => {
                                        function insertIntoCart(
                                          previousItems,
                                          currentItem
                                        ) {
                                          cartItems = [
                                            ...previousItems,
                                            currentItem,
                                          ];
                                          return cartItems;
                                        }
                                        this.prevCartItems =
                                          JSON.parse(
                                            localStorage.getItem("cartItems")
                                          ) != null
                                            ? JSON.parse(
                                              localStorage.getItem(
                                                "cartItems"
                                              )
                                            )
                                            : [];
                                        var currentCart = insertIntoCart(
                                          this.prevCartItems,
                                          product
                                        );
                                        // toast("Product Added to cart successfully");
                                        localStorage.setItem(
                                          "cartItems",
                                          JSON.stringify(currentCart)
                                        );
                                        setCartItems(currentCart);
                                      }}
                                    >
                                      <i className="fa fa-shopping" /> Add to
                                      Cart
                                    </button>
                                    <button
                                      onClick={(e) => {
                                        this.showModal(e);
                                      }}
                                      className="btn  btn-primary btn-inquiry cart"
                                    >
                                      {" "}
                                      Get Inquiry
                                    </button>
                                    <Modal size="lg" show={this.state.show} onHide={this.showModal}>
                                      <InquiryModal headerText="Enter Inquiry Details" onClose={this.showModal} show={this.state.show}>

                                        <EnquiryForm close={this.closeModal} />
                                      </InquiryModal>
                                    </Modal>
                                  </div>
                                </div>
                              ))}
                          </div>
                          <ul className="pagination">
                            {this.props.shopProducts &&
                              this.pageNumbers.map((number) => (
                                <li
                                  className="page-item"
                                  key={number}
                                  onClick={this.handleClick}
                                >
                                  <div
                                    className="page-link"
                                    id={number}
                                    href="#"
                                  >
                                    {number}
                                  </div>
                                </li>
                              ))}
                          </ul>
                        </>
                      ) : (
                        <div className="shopBanner">
                          <img
                            src="https://dummyimage.com/wxga"
                            className="w-100"
                          />
                          <h1>No products in this category</h1>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )}
      </AppContext.Consumer>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {state,shopProducts:state.shopProducts};
};

export default connect(mapStateToProps)(Shop);
