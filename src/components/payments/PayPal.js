import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { checkoutAPI } from 'services/checkout.service';
import config from '../../services/config';

export default function PayPal(props) {
    const [paid, setPaid] = useState(false);
    const [error, setError] = useState(null);

    const paypalRef = React.useRef();

    useEffect(() => {
        const { total, shippingInfo, productInfo } = props;
        const { first_name, last_name, address1, address2, city, country_name, state, zip_code, email, phone_number } = shippingInfo;
        const { RETURN_URL, CANCEL_URL } = config.PAYPAL;
        console.log(shippingInfo);

        window.paypal
            .Buttons({
                createOrder: (data, actions) => {
                    return actions.order.create({
                        intent: "CAPTURE",
                        application_context: {
                            brand_name: "RealTime",
                            locale: "en-IN",
                            user_action: "PAY_NOW",
                            return_url: RETURN_URL,
                            cancel_url: CANCEL_URL,
                            payment_method: {
                                payer_selected: "PAYPAL"
                            },
                            shipping_preferences: 'SET_PROVIDED_ADDRESS',
                        },
                        purchase_units: [
                            {
                                description: "Total amount to be paid is only ",
                                amount: {
                                    currency_code: "USD",
                                    value: total,
                                },
                                shipping: {
                                    name: {
                                        full_name: `${first_name} ${last_name}`
                                    },
                                    address: {
                                        address_line_1: `${address1}`,
                                        address_line_2: `${address2}`,
                                        admin_area_2: `${city}`,
                                        admin_area_1: `${state}`,
                                        postal_code: `${zip_code}`,
                                        country_code: `${country_name}`,
                                    }
                                }
                            },
                        ],
                        payer: {
                            name: {
                                given_name: `${first_name}`,
                                surname: `${last_name}`
                            },
                            email_address: `${email}`,
                            phone: {
                                phone_number: {
                                    national_number: "1234567890"
                                }
                            },
                            address: {
                                address_line_1: `${address1}`,
                                address_line_2: `${address2}`,
                                admin_area_2: `${city}`,
                                admin_area_1: `${state}`,
                                postal_code: `${zip_code}`,
                                country_code: `${country_name}`,
                            }
                        }
                    });
                },
                onApprove: async (data, actions) => {
                    const order = await actions.order.capture();
                    console.log(order);
                    productInfo.paypalTransId = order.id;
                    updateTranction(productInfo);
                },
                onError: (err) => {
                    setError(err),
                        console.error(err);
                },
            })
            .render(paypalRef.current);
    }, []);

    // If the payment has been made
    if (paid) {
        console.log("Payment Completed Successfully.")
        window.location.href = "/thankyou";

        // return (
        //     <div>
        //         <h2>Thanks for your order.</h2>
        //         <h4>Your payment of ${props.total} is complete.</h4>
        //     </div>);
    }

    // If any error occurs
    if (error) {
        return <div>Error Occurred in processing payment.! Please try again.</div>;
    }

    const updateTranction = (productInfo) => {
        checkoutAPI.submitPayment(productInfo)
            .then((response) => {
                console.log(response);
                setPaid(true);
            })
    }

    return (
        <div>
            <h3>REVIEW ORDER</h3>
            <hr />
            <p>You're almost done!</p>
            <p>Total Amount in USD. : ${props.total} /-</p>
            <div ref={paypalRef} />
        </div>
    )
}