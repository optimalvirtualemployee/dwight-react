import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import React from 'react';
import PayPal from './PayPal';

export default function PayPalDialog(props) {
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(!open);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        {/* <DialogTitle id="alert-dialog-title">{""}</DialogTitle> */}
        <DialogContent>
          <PayPal
            productInfo={props.productInfo}
            shippingInfo={props.shippingInfo}
            total={props.total} />
        </DialogContent>
        <DialogActions>
          {/* <Button onClick={handleClose} color="primary" autoFocus>
            OK
          </Button> */}
        </DialogActions>
      </Dialog>
    </div>
  );
}
