import React, { Component } from 'react';
import './ScrollToTop.css';

class ScrollToTop extends Component {
  // constructor(props){
    // super(props);
    // this.state = {};
  // }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  constructor(props) {
    super(props);
    this.state = {
      is_visible: true
    };
  }

  componentDidMount() {
    var scrollComponent = this;
    document.addEventListener("scroll", function(e) {
      scrollComponent.toggleVisibility();
    });
  }

  toggleVisibility() {
    if (window.pageYOffset > 300) {
      this.setState({
        is_visible: true
      });
    } else {
      this.setState({
        is_visible: false
      });
    }
  }

  scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }

  render() {
    const { is_visible } = this.state;
    return (
      <div className="scroll-to-top">
      {is_visible && (
        <button onClick={() => this.scrollToTop()} id="movetop" title="Go to top">
        <span className="fa fa-angle-up" aria-hidden="true"></span>
      </button>
      )}
      </div>
    );
  }
}

export default ScrollToTop;