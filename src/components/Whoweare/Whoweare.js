import React, { Component } from "react";
import "./Whoweare.css";

class Whoweare extends Component {
  // constructor(props){
  // super(props);
  // this.state = {};
  // }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    return (
      <div className="w3l-index3 pt-5 pb-5" id="about">
        <div className="container pt-lg-3">
          <div className="header-section text-center">
            <h3>Who We Are</h3>
            <p className="mt-3 mb-5">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti
              sit labore dolores aliquam possimus deleniti cupiditate
              temporibus, repudiandae suscipit asperiores vel.
            </p>
          </div>
          <div className="aboutgrids row">
            <div className="col-lg-6 aboutgrid1">
              <h4>Dream Big Inspire the World</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam
                id quisquam ipsam molestiae ad eius accusantium? Nulla dolorem
                perferendis inventore! posuere cubilia Curae; Nunc non risus in
                justo convallis feugiat.
              </p>

              <ul className="services-list mb-5">
                <li>Products & Solutions</li>
                <li>Dream big inspiring solutions</li>
                <li>Assured plant availability and operational security</li>
                <li>Improved operating conditions</li>
                <li>Relief of company's own maintenance resources</li>
              </ul>
              <a href="/#" className="btn btn-primary theme-button">
                More services
              </a>
            </div>
            <div className="col-lg-6 aboutgrid2 mt-lg-0 mt-5">
              <img
                src={process.env.PUBLIC_URL + "./assets/images/about.jpg"}
                alt="about"
                className="img-fluid"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Whoweare;
