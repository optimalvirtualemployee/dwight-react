import React, { Component } from 'react';
import './recommendation.css';
import { connect } from 'react-redux';
class Recommendation extends Component {

   // componentWillMount(){}
   // componentDidMount(){}
   // componentWillUnmount(){}

   // componentWillReceiveProps(){}
   // shouldComponentUpdate(){}
   // componentWillUpdate(){}
   // componentDidUpdate(){}

   render() {
      return (
         <section>
            <div className="row mt-3">
               <div className="col-lg-12">
                  <div className="inner">
                     <div className="border-bottom mt-2 p-3">
                        <span className="font-weight-bold">Recommendation</span>
                     </div>
                  </div>
               </div>
            </div>
            <div className="row">
               {this.props.recommendation.map((recommend, index) => (
                  <div key={index} className="col-xl-3 col-lg-4  col-md-6 offset-md-0 offset-sm-1 col-sm-10 offset-sm-1  my-2 mb-3">
                     <div className="card pBox ">
                        <img className="card-img-top" src={recommend.imgURL} alt="img"/>
                        <div className="card-body p-1">
                           <div className="d-flex align-items-start justify-content-between border-bottom mb-1">
                              <div className="d-flex flex-column">
                                 <div className="h6 font-weight-bold d-flex flex-column">
                                    {recommend.recName}
                                    <span>{recommend.dimension}</span>
                                   </div>
                                 <div className="text-muted">{recommend.recPrice}</div>
                              </div>
                              <div className="btn p-0"><span className="fa fa-heart" /></div>
                           </div>
                        </div>
                        <button className="btn btn-addToCart btn-long cart w-100"> <i className="fa fa-shopping" /> Add to Cart</button>
                     </div>
                  </div>
               ))}
            </div>

         </section>

      );
   }
}

const mapStateToProps = (state) => {
   return { recommendation: state.recommendation }
}

export default connect(mapStateToProps, {})(Recommendation);