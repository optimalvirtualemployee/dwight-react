import React, { Component } from 'react';
import './Features.css';
import { connect } from 'react-redux';
import { homeActions } from 'store/actions';
/*import {
  NavLink
} from "react-router-dom";*/
import { Link } from 'react-router-dom';
//import config from 'services/config';
//import { history } from 'store/helpers';
import Loader from 'layout/Loader/Loader';
class Features extends Component {

  constructor(props) {
    super(props);
    this.props.dispatch(homeActions.getServices())
    this.props.dispatch(homeActions.getFooter());
    this.serviceContent = null;
  }

  getServiceContentFromMenuManager = () => {
    var footerContentFilter = ['services-we-provide'];//Filter out based on slug from menu item
    var serviceContentArray = this.props.menuItems.filter(function (itm) {
      return footerContentFilter.indexOf(itm.page_slug) > -1;
    });
    serviceContentArray = { content: serviceContentArray };


    //console.log('serviceContentArray', serviceContentArray)
    this.serviceContent = serviceContentArray.content;
  }

  render() {
    if (this.props.footerMenu.loading || this.props.services.loading) return (<Loader/>);
    return (
      <section className="w3l-index1" id="services">
        {this.props.menuItems && this.getServiceContentFromMenuManager()}
        <div className="features-with-17_sur py-5">
          <div className="container py-lg-3">
            <div className="header-section text-center">
              {this.props.menuItems && (this.serviceContent.map((item) => (
                <React.Fragment key={item.page_slug}>
                  <h3>Services We Provide</h3>
                  <p className="mt-3 mb-5" dangerouslySetInnerHTML={{ __html: item.content }} />
                </React.Fragment>
              )))}


            </div>
            <div className="features-with-17-top_sur ">
              <div className="row">
                {this.props.services.items && this.props.services.items.results && this.props.services.items.results.map((service, index) => (

                  <div key={index} className="col-lg-4 col-md-6 m-top">
                    <div className="features-with-17-right-tp_sur border featurBox">
                      <div className="features-with-17-left1 mb-4">
                        <img src={service.image} alt={service.altText} className="w-100" />
                      </div>
                      <div className="features-with-17-left2 text-center">
                        <h6><a href="/#">{service.service_name}</a></h6>

                        <p className="text-truncate" dangerouslySetInnerHTML={{ __html: service.short_description.replace(/<[^>]+>/g, '') }} />
                        <Link to={`${service.link}`}><button className="btn btn-primary text-white mt-3">View More</button></Link>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div className="row mt-5">
                {/* <div className="col-md-12 text-center">
                      <button className="btn btn-primary text-white "><i className="fa fa-eye mr-2"></i>View More Services</button>
                  </div>                   */}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return { services: state.services,footerMenu:state.footerMenu, menuItems: state.footerMenu && state.footerMenu.menu ? state.footerMenu.menu : null };
}

export default connect(mapStateToProps)(Features);