import { Checkbox, FormControlLabel } from '@material-ui/core';
import React, { Component } from 'react';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
class Shipping extends Component {

    constructor(props) {
        super(props);
        this.sumbitHandler = this.sumbitHandler.bind(this);
    }

    sumbitHandler = (e) => {
        e.preventDefault();
        this.props.next(e);
    }

    render() {
        return (
            <>
                <div className="shipping-detail-box">
                    <form onSubmit={this.sumbitHandler} className="needs-validation" novalidate>
                        <div className="row">
                            <div className="col-md-6 mb-3">
                                <label >Country/Reigion *</label>
                                {/* <select onChange={this.props.onCountryChange} className="custom-select d-block w-100" id="country" required>
                                    <option selected value="CA">Canada</option>
                                    {[... new Set(this.props.countries)].map((country) =>
                                        <>
                                            <option key={country.id} value={country.code}>{country.name}</option>
                                        </>
                                    )}
                                </select> */}
                                <CountryDropdown
                                    className="custom-select d-block w-100"
                                    id="country" required
                                    valueType="short"
                                    value={this.props.getCountryValue(0)}
                                    priorityOptions={["CA", "US", "GB"]}
                                    onChange={(val) => this.props.selectCountry(0, val)}
                                />
                                <div className="invalid-feedback">
                                    Please select a valid country.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label >Order Type *</label>
                                <select onChange={this.props.onOrderTypeChange} id="order" className="custom-select d-block w-100" required>
                                    <option selected value="Company">Company</option>
                                    <option value="Personal">Personal</option>
                                </select>
                                <div className="invalid-feedback">
                                    Please select a valid OrderType.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label>First Name *</label>
                                <input type="text" id="fname" placeholder="John" className="form-control" value={this.props.fname}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    Valid first name is required.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label >Last Name *</label>
                                <input type="text" id="lname" placeholder="M. Doe" className="form-control" value={this.props.lname}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label >Company Name *</label>
                                <input type="text" id="companyName" placeholder="RealTime7 Inc." className="form-control" value={this.props.companyName}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label >Mail Stop</label>
                                <input type="text" id="mailStop" placeholder="UC Santa Cruz" className="form-control" value={this.props.mailStop}
                                    onChange={this.props.onChange} />
                                {/* <div className="invalid-feedback">
                                    Valid Mail Stop is required.
                                </div> */}
                            </div>
                            <div className="col-md-12 mb-3">
                                <label >Address Line 1 *</label>
                                <input type="text" placeholder="542 W. 15th Street" className="form-control" id="address1" value={this.props.address1}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    Please enter your address.
                                </div>
                            </div>

                            <div className="col-md-12 mb-3">
                                <label >Address Line 2<span className="text-muted">(Optional)</span></label>
                                <input type="text" placeholder="542 W. 15th Street" className="form-control" id="address2" value={this.props.address2}
                                    onChange={this.props.onChange} />
                                <div className="invalid-feedback">
                                    Please enter your address.
                                </div>
                            </div>
                            <div className="col-md-4 mb-3">
                                <label >City *</label>
                                <input type="text" placeholder="New York" className="form-control" id="city" value={this.props.city}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    City is required.
                                </div>
                            </div>
                            <div className="col-md-4 mb-3">
                                <label >Province *</label>
                                {/* <select onChange={this.props.onStateChange} className="custom-select d-block w-100" id="state" required>
                                    <option selected value="" disabled>Please Select</option>
                                    {[... new Set(this.props.states)].map((state) =>
                                        <>
                                            <option key={state.id} value={state.code}>{state.name}</option>
                                        </>
                                    )}
                                </select> */}
                                <RegionDropdown
                                    className="custom-select d-block w-100"
                                    id="state" required
                                    country={this.props.getCountryValue(0)}
                                    countryValueType="short"
                                    valueType="short"
                                    value={this.props.getRegionValue(0)}
                                    onChange={(val) => this.props.selectRegion(0, val)}
                                />
                                <div className="invalid-feedback">
                                    Please provide a valid state.
                                </div>
                            </div>
                            <div className="col-md-4 mb-3">
                                <label >Postal Code *</label>
                                <input type="text" placeholder="10001" className="form-control" id="zip" value={this.props.zip}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    ZipCode code required.
                                </div>
                            </div>
                        </div>
                        <div className="form-steps">
                            <h4 className="form-step-list">Your Contact Information</h4>
                        </div>

                        <div className="row mt-4">
                            <div className="col-md-12 mb-3">
                                <label >Email * </label>
                                <input type="email" onChange={this.props.onChange} className="form-control" value={this.props.email} id="email" placeholder="you@example.com" required />
                                <div className="invalid-feedback">
                                    Please enter a valid email address for shipping updates.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label >Website</label>
                                <input type="text" onChange={this.props.onChange} value={this.props.website} className="form-control" id="website" placeholder="www.xyz.com" />
                                <div className="invalid-feedback">
                                    Please enter a valid URL.
                                </div>
                            </div>
                            <div className="col-md-6 mb-3">
                                <label >Phone Number *</label>
                                <input type="text" placeholder="123-4567-8901" className="form-control" id="phone" value={this.props.phone}
                                    onChange={this.props.onChange} required />
                                <div className="invalid-feedback">
                                    Phone Number is required.
                                </div>
                            </div>
                        </div>
                        <hr className="mb-4" />
                        <div className="custom-checkbox">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        onChange={this.props.onSelectCheckbox}
                                        color="primary"
                                        checked={this.props.isSameAddress}
                                        name="isSameAddress"
                                    />
                                }
                                label="My Billing address is the same as my shipping"
                            />
                        </div>
                        <hr className="mb-4" />

                        <div className="form-steps">
                            <h4 className="form-step-list">Export</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                               printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
                        </div>

                        <div className="text-right  continue-btn">
                            <button className="btn btn-primary" type="submit">Continue to Shipping</button>
                        </div>
                    </form>
                </div>
            </>
        )
    }
}

export default Shipping;
