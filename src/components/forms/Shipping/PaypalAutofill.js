======================================Paypal shipping form auto fill==========================

1. create Countries.js and States.js inside forms/Shipping
a).for countries.js put below code
const countriesList = [
  { code: "AF", id: "004", name: "Afghanistan" },
  { code: "AL", id: "008", name: "Albania" },
  { code: "DZ", id: "012", name: "Algeria" },
  { code: "AS", id: "016", name: "American Samoa" },
  { code: "AD", id: "020", name: "Andorra" },
  { code: "AO", id: "024", name: "Angola" },
  { code: "AI", id: "660", name: "Anguilla" },
  { code: "AQ", id: "010", name: "Antarctica" },
  { code: "AG", id: "028", name: "Antigua and Barbuda" },
  { code: "AR", id: "032", name: "Argentina" },
  { code: "AM", id: "051", name: "Armenia" },
  { code: "AW", id: "533", name: "Aruba" },
  { code: "AU", id: "036", name: "Australia" },
  { code: "AT", id: "040", name: "Austria" },
  { code: "AZ", id: "031", name: "Azerbaijan" },
  { code: "BS", id: "044", name: "Bahamas (the)" },
  { code: "BH", id: "048", name: "Bahrain" },
  { code: "BD", id: "050", name: "Bangladesh" },
  { code: "BB", id: "052", name: "Barbados" },
  { code: "BY", id: "112", name: "Belarus" },
  { code: "BE", id: "056", name: "Belgium" },
  { code: "BZ", id: "084", name: "Belize" },
  { code: "BJ", id: "204", name: "Benin" },
  { code: "BM", id: "060", name: "Bermuda" },
  { code: "BT", id: "064", name: "Bhutan" },
  { code: "BO", id: "068", name: "Bolivia (Plurinational State of)" },
  { code: "BQ", id: "535", name: "Bonaire, Sint Eustatius and Saba" },
  { code: "BA", id: "070", name: "Bosnia and Herzegovina" },
  { code: "BW", id: "072", name: "Botswana" },
  { code: "BV", id: "074", name: "Bouvet Island" },
  { code: "BR", id: "076", name: "Brazil" },
  { code: "IO", id: "086", name: "British Indian Ocean Territory (the)" },
  { code: "BN", id: "096", name: "Brunei Darussalam" },
  { code: "BG", id: "100", name: "Bulgaria" },
  { code: "BF", id: "854", name: "Burkina Faso" },
  { code: "BI", id: "108", name: "Burundi" },
  { code: "CV", id: "132", name: "Cabo Verde" },
  { code: "KH", id: "116", name: "Cambodia" },
  { code: "CM", id: "120", name: "Cameroon" },
  { code: "CA", id: "124", name: "Canada" },
  { code: "KY", id: "136", name: "Cayman Islands (the)" },
  { code: "CF", id: "140", name: "Central African Republic (the)" },
  { code: "TD", id: "148", name: "Chad" },
  { code: "CL", id: "152", name: "Chile" },
  { code: "CN", id: "156", name: "China" },
  { code: "CX", id: "162", name: "Christmas Island" },
  { code: "CC", id: "166", name: "Cocos (Keeling) Islands (the)" },
  { code: "CO", id: "170", name: "Colombia" },
  { code: "KM", id: "174", name: "Comoros (the)" },
  { code: "CD", id: "180", name: "Congo (the Democratic Republic of the)" },
  { code: "CG", id: "178", name: "Congo (the)" },
  { code: "CK", id: "184", name: "Cook Islands (the)" },
  { code: "CR", id: "188", name: "Costa Rica" },
  { code: "HR", id: "191", name: "Croatia" },
  { code: "CU", id: "192", name: "Cuba" },
  { code: "CW", id: "531", name: "Curaçao" },
  { code: "CY", id: "196", name: "Cyprus" },
  { code: "CZ", id: "203", name: "Czechia" },
  { code: "CI", id: "384", name: "Côte d'Ivoire" },
  { code: "DK", id: "208", name: "Denmark" },
  { code: "DJ", id: "262", name: "Djibouti" },
  { code: "DM", id: "212", name: "Dominica" },
  { code: "DO", id: "214", name: "Dominican Republic (the)" },
  { code: "EC", id: "218", name: "Ecuador" },
  { code: "EG", id: "818", name: "Egypt" },
  { code: "SV", id: "222", name: "El Salvador" },
  { code: "GQ", id: "226", name: "Equatorial Guinea" },
  { code: "ER", id: "232", name: "Eritrea" },
  { code: "EE", id: "233", name: "Estonia" },
  { code: "SZ", id: "748", name: "Eswatini" },
  { code: "ET", id: "231", name: "Ethiopia" },
  { code: "FK", id: "238", name: "Falkland Islands (the) [Malvinas]" },
  { code: "FO", id: "234", name: "Faroe Islands (the)" },
  { code: "FJ", id: "242", name: "Fiji" },
  { code: "FI", id: "246", name: "Finland" },
  { code: "FR", id: "250", name: "France" },
  { code: "GF", id: "254", name: "French Guiana" },
  { code: "PF", id: "258", name: "French Polynesia" },
  { code: "TF", id: "260", name: "French Southern Territories (the)" },
  { code: "GA", id: "266", name: "Gabon" },
  { code: "GM", id: "270", name: "Gambia (the)" },
  { code: "GE", id: "268", name: "Georgia" },
  { code: "DE", id: "276", name: "Germany" },
  { code: "GH", id: "288", name: "Ghana" },
  { code: "GI", id: "292", name: "Gibraltar" },
  { code: "GR", id: "300", name: "Greece" },
  { code: "GL", id: "304", name: "Greenland" },
  { code: "GD", id: "308", name: "Grenada" },
  { code: "GP", id: "312", name: "Guadeloupe" },
  { code: "GU", id: "316", name: "Guam" },
  { code: "GT", id: "320", name: "Guatemala" },
  { code: "GG", id: "831", name: "Guernsey" },
  { code: "GN", id: "324", name: "Guinea" },
  { code: "GW", id: "624", name: "Guinea-Bissau" },
  { code: "GY", id: "328", name: "Guyana" },
  { code: "HT", id: "332", name: "Haiti" },
  { code: "HM", id: "334", name: "Heard Island and McDonald Islands" },
  { code: "VA", id: "336", name: "Holy See (the)" },
  { code: "HN", id: "340", name: "Honduras" },
  { code: "HK", id: "344", name: "Hong Kong" },
  { code: "HU", id: "348", name: "Hungary" },
  { code: "IS", id: "352", name: "Iceland" },
  { code: "IN", id: "356", name: "India" },
  { code: "ID", id: "360", name: "Indonesia" },
  { code: "IR", id: "364", name: "Iran (Islamic Republic of)" },
  { code: "IQ", id: "368", name: "Iraq" },
  { code: "IE", id: "372", name: "Ireland" },
  { code: "IM", id: "833", name: "Isle of Man" },
  { code: "IL", id: "376", name: "Israel" },
  { code: "IT", id: "380", name: "Italy" },
  { code: "JM", id: "388", name: "Jamaica" },
  { code: "JP", id: "392", name: "Japan" },
  { code: "JE", id: "832", name: "Jersey" },
  { code: "JO", id: "400", name: "Jordan" },
  { code: "KZ", id: "398", name: "Kazakhstan" },
  { code: "KE", id: "404", name: "Kenya" },
  { code: "KI", id: "296", name: "Kiribati" },
  { code: "KP", id: "408", name: "Korea (the Democratic People's Republic of)" },
  { code: "KR", id: "410", name: "Korea (the Republic of)" },
  { code: "KW", id: "414", name: "Kuwait" },
  { code: "KG", id: "417", name: "Kyrgyzstan" },
  { code: "LA", id: "418", name: "Lao People's Democratic Republic (the)" },
  { code: "LV", id: "428", name: "Latvia" },
  { code: "LB", id: "422", name: "Lebanon" },
  { code: "LS", id: "426", name: "Lesotho" },
  { code: "LR", id: "430", name: "Liberia" },
  { code: "LY", id: "434", name: "Libya" },
  { code: "LI", id: "438", name: "Liechtenstein" },
  { code: "LT", id: "440", name: "Lithuania" },
  { code: "LU", id: "442", name: "Luxembourg" },
  { code: "MO", id: "446", name: "Macao" },
  { code: "MG", id: "450", name: "Madagascar" },
  { code: "MW", id: "454", name: "Malawi" },
  { code: "MY", id: "458", name: "Malaysia" },
  { code: "MV", id: "462", name: "Maldives" },
  { code: "ML", id: "466", name: "Mali" },
  { code: "MT", id: "470", name: "Malta" },
  { code: "MH", id: "584", name: "Marshall Islands (the)" },
  { code: "MQ", id: "474", name: "Martinique" },
  { code: "MR", id: "478", name: "Mauritania" },
  { code: "MU", id: "480", name: "Mauritius" },
  { code: "YT", id: "175", name: "Mayotte" },
  { code: "MX", id: "484", name: "Mexico" },
  { code: "FM", id: "583", name: "Micronesia (Federated States of)" },
  { code: "MD", id: "498", name: "Moldova (the Republic of)" },
  { code: "MC", id: "492", name: "Monaco" },
  { code: "MN", id: "496", name: "Mongolia" },
  { code: "ME", id: "499", name: "Montenegro" },
  { code: "MS", id: "500", name: "Montserrat" },
  { code: "MA", id: "504", name: "Morocco" },
  { code: "MZ", id: "508", name: "Mozambique" },
  { code: "MM", id: "104", name: "Myanmar" },
  { code: "NA", id: "516", name: "Namibia" },
  { code: "NR", id: "520", name: "Nauru" },
  { code: "NP", id: "524", name: "Nepal" },
  { code: "NL", id: "528", name: "Netherlands (the)" },
  { code: "NC", id: "540", name: "New Caledonia" },
  { code: "NZ", id: "554", name: "New Zealand" },
  { code: "NI", id: "558", name: "Nicaragua" },
  { code: "NE", id: "562", name: "Niger (the)" },
  { code: "NG", id: "566", name: "Nigeria" },
  { code: "NU", id: "570", name: "Niue" },
  { code: "NF", id: "574", name: "Norfolk Island" },
  { code: "MP", id: "580", name: "Northern Mariana Islands (the)" },
  { code: "NO", id: "578", name: "Norway" },
  { code: "OM", id: "512", name: "Oman" },
  { code: "PK", id: "586", name: "Pakistan" },
  { code: "PW", id: "585", name: "Palau" },
  { code: "PS", id: "275", name: "Palestine, State of" },
  { code: "PA", id: "591", name: "Panama" },
  { code: "PG", id: "598", name: "Papua New Guinea" },
  { code: "PY", id: "600", name: "Paraguay" },
  { code: "PE", id: "604", name: "Peru" },
  { code: "PH", id: "608", name: "Philippines (the)" },
  { code: "PN", id: "612", name: "Pitcairn" },
  { code: "PL", id: "616", name: "Poland" },
  { code: "PT", id: "620", name: "Portugal" },
  { code: "PR", id: "630", name: "Puerto Rico" },
  { code: "QA", id: "634", name: "Qatar" },
  { code: "MK", id: "807", name: "Republic of North Macedonia" },
  { code: "RO", id: "642", name: "Romania" },
  { code: "RU", id: "643", name: "Russian Federation (the)" },
  { code: "RW", id: "646", name: "Rwanda" },
  { code: "RE", id: "638", name: "Réunion" },
  { code: "BL", id: "652", name: "Saint Barthélemy" },
  { code: "SH", id: "654", name: "Saint Helena, Ascension and Tristan da Cunha" },
  { code: "KN", id: "659", name: "Saint Kitts and Nevis" },
  { code: "LC", id: "662", name: "Saint Lucia" },
  { code: "MF", id: "663", name: "Saint Martin (French part)" },
  { code: "PM", id: "666", name: "Saint Pierre and Miquelon" },
  { code: "VC", id: "670", name: "Saint Vincent and the Grenadines" },
  { code: "WS", id: "882", name: "Samoa" },
  { code: "SM", id: "674", name: "San Marino" },
  { code: "ST", id: "678", name: "Sao Tome and Principe" },
  { code: "SA", id: "682", name: "Saudi Arabia" },
  { code: "SN", id: "686", name: "Senegal" },
  { code: "RS", id: "688", name: "Serbia" },
  { code: "SC", id: "690", name: "Seychelles" },
  { code: "SL", id: "694", name: "Sierra Leone" },
  { code: "SG", id: "702", name: "Singapore" },
  { code: "SX", id: "534", name: "Sint Maarten (Dutch part)" },
  { code: "SK", id: "703", name: "Slovakia" },
  { code: "SI", id: "705", name: "Slovenia" },
  { code: "SB", id: "090", name: "Solomon Islands" },
  { code: "SO", id: "706", name: "Somalia" },
  { code: "ZA", id: "710", name: "South Africa" },
  { code: "GS", id: "239", name: "South Georgia and the South Sandwich Islands" },
  { code: "SS", id: "728", name: "South Sudan" },
  { code: "ES", id: "724", name: "Spain" },
  { code: "LK", id: "144", name: "Sri Lanka" },
  { code: "SD", id: "729", name: "Sudan (the)" },
  { code: "SR", id: "740", name: "Suriname" },
  { code: "SJ", id: "744", name: "Svalbard and Jan Mayen" },
  { code: "SE", id: "752", name: "Sweden" },
  { code: "CH", id: "756", name: "Switzerland" },
  { code: "SY", id: "760", name: "Syrian Arab Republic" },
  { code: "TW", id: "158", name: "Taiwan" },
  { code: "TJ", id: "762", name: "Tajikistan" },
  { code: "TZ", id: "834", name: "Tanzania, United Republic of" },
  { code: "TH", id: "764", name: "Thailand" },
  { code: "TL", id: "626", name: "Timor-Leste" },
  { code: "TG", id: "768", name: "Togo" },
  { code: "TK", id: "772", name: "Tokelau" },
  { code: "TO", id: "776", name: "Tonga" },
  { code: "TT", id: "780", name: "Trinidad and Tobago" },
  { code: "TN", id: "788", name: "Tunisia" },
  { code: "TR", id: "792", name: "Turkey" },
  { code: "TM", id: "795", name: "Turkmenistan" },
  { code: "TC", id: "796", name: "Turks and Caicos Islands (the)" },
  { code: "TV", id: "798", name: "Tuvalu" },
  { code: "UG", id: "800", name: "Uganda" },
  { code: "UA", id: "804", name: "Ukraine" },
  { code: "AE", id: "784", name: "United Arab Emirates (the)" },
  { code: "GB", id: "826", name: "United Kingdom of Great Britain and Northern Ireland (the)" },
  { code: "UM", id: "581", name: "United States Minor Outlying Islands (the)" },
  { code: "US", id: "840", name: "United States" },
  { code: "UY", id: "858", name: "Uruguay" },
  { code: "UZ", id: "860", name: "Uzbekistan" },
  { code: "VU", id: "548", name: "Vanuatu" },
  { code: "VE", id: "862", name: "Venezuela (Bolivarian Republic of)" },
  { code: "VN", id: "704", name: "Viet Nam" },
  { code: "VG", id: "092", name: "Virgin Islands (British)" },
  { code: "VI", id: "850", name: "Virgin Islands (U.S.)" },
  { code: "WF", id: "876", name: "Wallis and Futuna" },
  { code: "EH", id: "732", name: "Western Sahara" },
  { code: "YE", id: "887", name: "Yemen" },
  { code: "ZM", id: "894", name: "Zambia" },
  { code: "ZW", id: "716", name: "Zimbabwe" },
  { code: "AX", id: "248", name: "Åland Islands" }
];

export default countriesList;

b). for states.js put below code

const stateList = [
    { code: "AB", id: "004", name: "Alberta" },
    { code: "BC", id: "008", name: "British Columbia" },
    { code: "MB", id: "012", name: "Manitoba" },
    { code: "NB", id: "016", name: "New Brunswick" },
    { code: "NL", id: "020", name: "Northwest Territories" },
    { code: "NT", id: "024", name: "Angola" },
    { code: "NS", id: "660", name: "Nova Scotia" },
    { code: "NU", id: "010", name: "Nunavut" },
    { code: "ON", id: "028", name: "Ontario" },
    { code: "PE", id: "032", name: "Prince Edward Island" },
    { code: "QC", id: "051", name: "Quebec" },
    { code: "SK", id: "032", name: "Saskatchewan" },
    { code: "YT", id: "051", name: "Yukon Territory" },
];
export default stateList;

2. add below import statements inside Multistepform.js 

import countriesList from '../../../src/components/forms/Shipping/Countries';
import stateList from '../../../src/components/forms/Shipping/States';

and  country: 'CA', // inside state variable

3. go to shipping.js replace state and countries options

 <option key={state.id} value={state.code}>{state.name}</option> // for states
 
  <select onChange={this.props.onCountryChange} className="custom-select d-block w-100" id="country" required>
	<option selected value="CA">Canada</option>
	{[... new Set(this.props.countries)].map((country) =>
		<>
			<option key={country.id} value={country.code}>{country.name}</option>
		</>
	)}
 </select>
 
4. go to the paypal.js and replace createOrder function 

 createOrder: (data, actions) => {
                    return actions.order.create({
                        intent: "CAPTURE",
                        application_context: {
                          brand_name: "RealTime",
                            locale: "en-IN",
                            user_action: "PAY_NOW",
                            return_url: RETURN_URL,
                            cancel_url: CANCEL_URL,
                            payment_method: {
                                payer_selected: "PAYPAL"
                            }
                        },
                        purchase_units: [
                            {
                                description: "Total amount to be paid is only ",
                                amount: {
                                    currency_code: "USD",
                                    value: total,
                                },
                                shipping: {
                                    name: {
                                        full_name: `${first_name} ${last_name}`
                                    },
                                    address: {
                                        address_line_1: `${address1}`,
                                        address_line_2: `${address2}`,
                                        admin_area_2: `${city}`,
                                        admin_area_1: `${state}`,
                                        postal_code: `${zip_code}`,
                                        country_code: `${country_name}`,
                                    }
                                }
                            },
                        ],
                        payer: {
                            name: {
                                given_name: `${first_name}`,
                                surname: `${last_name}`
                            },
                            email_address: `${email}`,
                            phone: {
                                phone_number: {
                                    national_number: "1234567890"
                                }
                            },
                            address: {
                                address_line_1: `${address1}`,
                                address_line_2: `${address2}`,
                                admin_area_2: `${city}`,
                                admin_area_1: `${state}`,
                                postal_code: `${zip_code}`,
                                country_code: `${country_name}`,
                            }
                        }
                    });
                },
5. inside paypal.js add 

import config from '../../services/config';
const { RETURN_URL, CANCEL_URL } = config.PAYPAL;

6. go to config.js add

 PAYPAL: {
        RETURN_URL: 'http://localhost/thankyou',
        CANCEL_URL: 'http://localhost/thankyou'
    },

7. On CheckoutPage update getShippingCharges method

 getShippingCharges = (shippingDetails) => {
    window.scrollTo(0, 200);
    if (this.isCartItems === true) {
      trackPromise(
        checkoutAPI.submitData(shippingDetails)
          .then((responseData) => {

            let shippingWays = [];
            if (responseData != null && responseData.status === 200 && responseData.getRate.length > 0) {

              responseData.getRate.map((data, index) => {
                shippingWays.push({
                  id: index,
                  name: data.service_name,
                  value: data.service_name,
                  amount: parseFloat(data.price)
                })
              })
              localStorage.setItem("shipAmt", parseFloat(responseData.getRate[0].price));
              let total = parseFloat(responseData.getRate[0].price) + parseFloat(this.cartItems.total)
              localStorage.setItem("total", parseFloat(total));
              this.setState({ shippingAmount: responseData.getRate[0].price })
              this.setState({
                isShippingNotAvailable: false,//!this.state.isShippingNotAvailable,
                shippingWays: shippingWays,
                shippingAddress: shippingDetails,
                shippingId: responseData.shipping_id
              })
            }
            else {
              if (responseData !== null && responseData.hasOwnProperty('error')) {
                alert(responseData.error[0])
              }
            }
          })
      );
    }
    else {
      alert("There are No items in the cart.");
      localStorage.removeItem("total");
      localStorage.removeItem("shipAmt");
    }
  }	
	
8. On checkout page update li tag you can find after <Spinner />

 <li className={this.state.isShippingNotAvailable ? "hide-shipping-item" : "list-group-item d-flex justify-content-between align-items-center px-0"}>
	Shipping
  {/* <span>Gratis</span> */}
	{this.isCartItems = true}
	{this.cartItems = cartItems}
	<span>
	  <select value={this.state.shippingAmount} onChange={this.onChangeShippingMethod} className="custom-select d-block w-100" id="country" required>
		{this.state.shippingWays != null && [... new Set(this.state.shippingWays)].map((item) =>
		  <>
			<option key={item.id} value={item.amount}>{`${item.name} $${item.amount}`}</option>
		  </>
		)}
	  </select>
	</span>
  </li>
							 
9. add below line in the constructor of checkoutpage.js
this.isCartItems = false;

10. run npm i react-country-region-selector

11. add these methods in multistepform.js

 getCountryValue(index) {
    return this.state.country;
  }

  getRegionValue(index) {
    return this.state.state;
  }

  selectCountry(exampleIndex, val) {
    this.setState({ country: val })
  }

  selectRegion(exampleIndex, val) {
    this.setState({ state: val })
  }
  
 add below props in <shipping />
 
  selectCountry={this.selectCountry.bind(this)}
  selectRegion={this.selectRegion.bind(this)}
  getCountryValue={this.getCountryValue.bind(this)}
  getRegionValue={this.getRegionValue.bind(this)}