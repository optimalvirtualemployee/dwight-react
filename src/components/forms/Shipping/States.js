const stateList = [
    { code: "AB", id: "004", name: "Alberta" },
    { code: "BC", id: "008", name: "British Columbia" },
    { code: "MB", id: "012", name: "Manitoba" },
    { code: "NB", id: "016", name: "New Brunswick" },
    { code: "NL", id: "020", name: "NewfoundLand and Labrador" },
    { code: "NT", id: "024", name: "Northwest Territories" },
    { code: "NS", id: "660", name: "Nova Scotia" },
    { code: "NU", id: "010", name: "Nunavut" },
    { code: "ON", id: "028", name: "Ontario" },
    { code: "PE", id: "032", name: "Prince Edward Island" },
    { code: "QC", id: "051", name: "Quebec" },
    { code: "SK", id: "032", name: "Saskatchewan" },
    { code: "YT", id: "051", name: "Yukon Territory" },
];
export default stateList;
