import React, { Component } from 'react';
//import { NavLink } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './Slider.css';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import { connect } from "react-redux";
import { homeActions } from 'store/actions/home.actions';
//import config from './../../services/config'
//import { history } from 'store/helpers';
//import { homeConstants } from 'store/constants';
import Loader from 'layout/Loader/Loader';

class Slider extends Component {

  constructor(props) {
    super(props);
    this.props.dispatch(homeActions.getSliders())
  }
  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    if (this.props.home.loading) return (<Loader />);
    return (

      <section className="w3l-main-slider">

        <div className="companies20-content">
          <div className="companies-wrapper" style={{ display: "flex" }}>
            <Carousel className='owl-one owl-carousel owl-theme text-center'
              centerSlidePercentage={90}
              showStatus={false}
              showThumbs={false}
              autoPlay={false}
              infiniteLoop
              dynamicHeight={false}
              swipeable={true}
            >
              {this.props.home.items && this.props.home.items.results && this.props.home.items.results.map((slider, index) => (
                <ul className="item" key={index}>
                  <li>
                    <div className='slider-info  banner-view'>
                      <img src={slider.image} alt="img" />
                      <div className="banner-info">
                        {/* <h3 className="banner-text">{slider.heading}</h3>
                        <p className="slidetwo mb-3">{slider.description}</p> */}

                        {/* <a href="/#" className="btn btn-primary theme-button">{slider.service_description }</a> */}
                        <Link to={slider.link} className="btn btn-primary theme-button">View more</Link>
                      </div>
                    </div>
                  </li>
                </ul>
              ))}
            </Carousel>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
}
export default connect(mapStateToProps)(Slider);
