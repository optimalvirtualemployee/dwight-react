import React, { Component } from "react";
import { connect } from 'react-redux';
import { homeActions } from 'store/actions';
import './Newsletter.css';
class Newsletter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      msg: 'Seems good.',
      isError: {
        email: "Please enter email. It is required to subscribe.",
      },
    };

    this.regExp = RegExp(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/);
  }

  updateMessage = () => {
    if (this.state.email) {
      if (this.props.subscribers.items) {
        if (this.props.subscribers.items.error) {
          this.setState({
            msg: this.props.subscribers.items.error
          });
        } else {
          document.getElementsByName('email').value = '';
          this.setState({
            email: "",
            msg: "Yay!! You're now subscribed to the newsletter. Welcome to the family."
          })
        }
      }
    }
  }


  formValid = ({ isError, ...rest }) => {
    let isValid = false;

    Object.values(isError).forEach((val) => {
      if (val.length > 0) {
        isValid = false;
      } else {
        isValid = true;
      }
    });

    Object.values(rest).forEach((val) => {
      if (val === null) {
        isValid = false;
      } else {
        isValid = true;
      }
    });

    return isValid;
  };



  onSubmit = (e) => {
    e.preventDefault();

    if (this.formValid(this.state)) {
      e.target.className += " was-validated";
      e.target.reset();
      this.props.dispatch(homeActions.newsletter(this.state.email, true));
      setTimeout(
        function () {
          this.updateMessage()
        }
          .bind(this),
        500
      );
    } else {
      console.log("Form is invalid!");
    }
  };

  formValChange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    let isError = { ...this.state.isError };
    switch (name) {
      case "email":
        isError.email = this.regExp.test(value)
          ? ""
          : "Email address is invalid";
        break;
      default:
        break;
    }
    this.setState({
      isError,
      [name]: value,
    });
  };

  render() {
    const { isError } = this.state;

    return (
      <>
        <section className="w3l-subscribe">
          <div className="main-w3 py-5">
            <div className="container py-lg-3">
              <div className="grids-forms text-center">
                <div className="row">
                  <div className="col-md-12">
                    <span className="fa fa-envelope mb-3" aria-hidden="true"></span>
                    <div className="header-section">
                      <h3>Keep up to date - Get Email updates</h3>
                      <p>Stay tuned for the latest company news.</p>
                    </div>
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-md-7 mx-auto main-midd-2">
                    <form action="#" method="post" className=" rightside-form needs-validation position-relative" onSubmit={this.onSubmit} noValidate>
                      <input required type="email" onChange={this.formValChange} name="email" placeholder="Your Email Address.." className={
                        isError.email.length > 0 ? "is-invalid mb-0" : "mb-0"
                      }  />
                      <button type="submit" className="btn btn-primary theme-button">Subscribe</button>
                      <div className="valid-feedback newsletterError">
                        {this.state.email.length>0 &&  this.state.msg}
                      </div>
                      {isError.email.length > 0 && (
                        <div className="invalid-feedback newsletterError pull-left">{isError.email}</div>
                      )}

                      {isError.email.length == 0 && (
                        <div className="invalid-feedback newsletterError pull-left">{isError.email}</div>
                      )}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </>
    );
  }
}


const mapStateToProps = (state) => {
  return state;
}
export default connect(mapStateToProps)(Newsletter);