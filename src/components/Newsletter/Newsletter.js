import React, { Component, createRef } from "react";
import { connect } from "react-redux";
import { homeActions } from "store/actions";
import "./Newsletter.css";
let formIsValid = true;
class Newsletter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      errors: {},
      success: {},
      newsletter: this.props.newsletter,
    };
    this.buttonRef = createRef();
  }


  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
    this.buttonRef.current.disabled = false;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.buttonRef.current.disabled = true; 
    this.props.dispatch(homeActions.newsletter(this.state.email, true))
    if (this.validateForm()) {
      setTimeout(() => {
        if (
          this.props.subscribers &&
          this.props.subscribers.items && this.props.subscribers.items.error
        ) {
          //alert('2')
          let errors = {};
          errors["email"] = this.props.subscribers.items.error;
          this.setState({ errors });
          this.setState({ success: "" });
          this.buttonRef.current.disabled = false;
        } else {
          this.setState({ email: "" });
          let success = {};
          success["email"] =
            "Yay!! You're now subscribed to the newsletter. Welcome to the family.";
          this.setState({ success });
          this.buttonRef.current.disabled = false;
        }
      }
        , 1000)
    }
  };

  validateForm = () => {
    let errors = {};
    if (!this.state.email) {
      formIsValid = false;
      errors["email"] = "*Please enter your email";
    }
    if (this.state.email) {
      //regular expression for email validation
      let pattern = new RegExp(
        /^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(this.state.email)) {
        formIsValid = false;
        errors["email"] = "*Please enter valid email";
      }
    }

    this.setState({ errors });
    return formIsValid;
  };

  render() {
    const { email } = this.state;

    return (
      <section className="w3l-subscribe">
        <div className="main-w3 py-5">
          <div className="container py-lg-3">
            <div className="grids-forms text-center">
              <div className="row">
                <div className="col-md-12">
                  <span
                    className="fa fa-envelope mb-3"
                    aria-hidden="true"
                  ></span>
                  <div className="header-section">
                    <h3>Keep up to date - Get Email updates</h3>
                    <p>Stay tuned for the latest company news.</p>
                  </div>
                </div>
              </div>
              <div className="row mt-5">
                <div className="col-md-7 mx-auto main-midd-2">
                  <form
                    onSubmit={this.handleSubmit}
                    className="rightside-form needs-validation position-relative"
                  >
                    <input
                      type="email"
                      name="email"
                      value={email}
                      onChange={this.handleChange}
                      className="input"
                    />

                    {/* <input required type="email" onChange={this.handleChange} name="email" placeholder="Your Email Address.." className={
                        this.state.errors.email > 0 ? "is-invalid mb-0" : "mb-0"
                      }  /> */}

                    <button
                      type="submit"
                      className="btn btn-primary theme-button"
                      ref={this.buttonRef}
                    >
                      Subscribe
                    </button>
                    <div className="errorMsg">{this.state.errors.email}</div>
                    <div className="successMsg">{this.state.success.email}</div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};
export default connect(mapStateToProps)(Newsletter);
