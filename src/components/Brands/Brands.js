import React, { Component } from 'react';
import './Brands.css';

class Brands extends Component {
  // constructor(props){
    // super(props);
    // this.state = {};
  // }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    return (
      <section className="w3l-logos py-5">
          <div className="container py-lg-3">
            <div className="header-section text-center">
              <h3>Over 12k+ Companies trust us</h3>
              <p className="mt-3 mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti sit labore dolores aliquam possimus deleniti cupiditate temporibus, repudiandae suscipit asperiores vel.</p>
            </div>
            <div className="row">
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo1.jpg'} alt="company-logo" className="img-fluid" />
              </div>
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo2.jpg'} alt="company-logo" className="img-fluid" />
              </div>
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo3.jpg'} alt="company-logo" className="img-fluid" />
              </div>
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo4.jpg'} alt="company-logo" className="img-fluid" />
              </div>
            </div>
            <div className="row mt-lg-4 mt-0">
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo3.jpg'} alt="company-logo" className="img-fluid" />
              </div>
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo4.jpg'} alt="company-logo" className="img-fluid" />
              </div>
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo1.jpg'} alt="company-logo" className="img-fluid" />
              </div>
              <div className="col-lg-3 col-6 logo-view">
                <img src={process.env.PUBLIC_URL +'./assets/images/logo2.jpg'} alt="company-logo" className="img-fluid" />
              </div>
            </div>
          </div>
        </section>
    );
  }
}

export default Brands;