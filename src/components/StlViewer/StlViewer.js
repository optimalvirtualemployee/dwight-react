import React, { Component } from "react";
import * as THREE from "three/build/three.module.js";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
var STLLoader = require("three-stl-loader")(THREE);
const style = {
  height: 400,
  width: '100%',
  // marginLeft:-75 // we can control scene size by setting container dimensions
};
//let selectedFace = "";
let stlUrl = "./../assets/stl/slotted_disk.stl";
/*let defaultTextureUrl =
  process.env.PUBLIC_URL + "./../assets/textures/ceramic.jpeg";
*/
class StlViewer extends Component {
  constructor(props) {
    super(props);
    this.child = React.createRef();
  }

  onTextureClick = (e) => {
    this.child.current.applyTexture(e);
  };

  onDimensionClick = (e) => {
    this.child.current.changeDimension(e);
  };

  onChangeUnitClick = (e) => {
    this.child.current.changeUnit(e);
  };

  onOrientationClick = (e) => {
    this.child.current.setOrientation(e);
  };

  importFromFileBodyComponent = () => {
    const handleFileChosen = (file) => {
      var tmppath = URL.createObjectURL(file);
      stlUrl = tmppath;
      this.child.current.addCustomSceneObjects();
    };

    return (
      <div className="upload-expense">
        <input
          type="file"
          id="file"
          className="input-file"
          accept=".stl"
          onChange={(e) => {
            this.setState({ showLoading: true });
            handleFileChosen(e.target.files[0]);
          }}
        />
      </div>
    );
  };

  render() {
    return (
      <>
        <section>
          <div className="productView">
            <div className="inner">
              <div className="productTop p-3">
                <div className="row">
                  <div className="col-lg-3">
                    <div>
                      <div className="metricfilter">
                        <button className="btn-metric">Metric</button>
                        <select
                          name
                          id="unit-select"
                          onChange={(e) => {
                            this.onChangeUnitClick(e.target.value);
                          }}
                          className="p-1"
                        >
                          <option value="mm" defaultValue>
                            mm
                          </option>
                          <option value="in">inches</option>
                        </select>
                      </div>
                      <div className="imgCn mt-3 mb-3">
                        <button
                          className="btn-front btn btn-success mr-2"
                          onClick={() => {
                            this.onOrientationClick("front");
                          }}
                        >
                          Front
                        </button>
                        <button
                          className="btn-top btn btn-success"
                          onClick={() => {
                            this.onOrientationClick("top");
                          }}
                        >
                          Top
                        </button>
                      </div>
                      {/* <div className="slidecontainer" style={{ margin: "5px" }}>
                        <p>Change Volume</p>
                        <input
                          type="range"
                          id="in-dim-percent"
                          min={0}
                          max={100}
                          defaultValue={100}
                          onInput={(e) => this.onDimensionClick(e)}
                        />
                        <span
                          id="op-dim-percent"
                          style={{ color: "white", margin: "5px" }}
                        >
                          100%
                        </span>
                      </div> */}
                      <table>
                        <tbody>
                          <tr id="model-dimension" className="theme-border">
                            <td>Width:</td>
                            <td>0</td>
                            <td>Height:</td>
                            <td>0</td>
                            <td>Depth:</td>
                            <td>0</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="col-lg-6 text-center">
                    <STLElement ref={this.child} />
                  </div>
                  <div className="col-lg-3">
                    <div className="table-responsive">
                      <table className="w-100">
                        <thead>
                          <tr>
                            <th />
                            <th>Units</th>
                            <th>Line Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Machine Time</td>
                            <td>4hrs</td>
                            <td>240.00</td>
                          </tr>
                          <tr>
                            <td>Material Copper</td>
                            <td>122g</td>
                            <td>12.54</td>
                          </tr>
                          <tr>
                            <td>Shipping</td>
                            <td />
                            <td>22.00</td>
                          </tr>
                          <tr>
                            <td>Tax</td>
                            <td />
                            <td>00.00</td>
                          </tr>
                          <tr>
                            <td />
                            <td>USD$</td>
                            <td>274.54</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="text-center mt-5">
                      <button className="btn btn-primary">
                        {" "}
                        <i className="fa fa-shopping-cart mr-2"> </i>Purchase
                        Now
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="productFilter p-3 row m-0">
                <div className="service col-lg-3 tooltip2">
                  <label> Services</label>
                  <select name="service" id className="form-control">
                    <option value="service 1">Service 1</option>
                    <option value="service 1">Service 2</option>
                  </select>
                  <span className="tooltiptext">Tooltip text</span>
                </div>
                <div className="material col-lg-3 tooltip2">
                  <label> Material</label>
                  <select
                    name="material"
                    id="material"
                    className="form-control"
                    onChange={(e) => {
                      this.onTextureClick(e);
                    }}
                  >
                    <option value>Select Material</option>
                    <option value="m1">Copper</option>
                    <option value="m2">Ceramic</option>
                    <option value="m3">Almunium</option>
                  </select>
                  <span className="tooltiptext">Tooltip text</span>
                </div>
                <div className="shipping col-lg-3 tooltip2">
                  <label> Shipping</label>
                  <select name="countryShipping" id className="form-control">
                    <option value="m1">India</option>
                    <option value="m2">Australia</option>
                    <option value="m2">The United States</option>
                  </select>
                  <span className="tooltiptext">Tooltip text</span>
                </div>
                <div className="uploadCustom col-lg-3 tooltip2">
                  <label> Upload Custom File</label>
                  {/* <input
                  type="file"
                  name="uploadCustom"
                  id="stl-model-input"
                  className="form-control"
                  
                /> */}
                  {this.importFromFileBodyComponent()}
                  <span className="tooltiptext">Tooltip text</span>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

class STLElement extends Component {
  componentDidMount() {
    this.sceneSetup();
    this.addCustomSceneObjects();
    this.startAnimationLoop();
    window.addEventListener("resize", this.handleWindowResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowResize);
    window.cancelAnimationFrame(this.requestID);
    this.controls.dispose();
  }

  // Standard scene setup in Three.js. Check "Creating a scene" manual for more information
  // https://threejs.org/docs/#manual/en/introduction/Creating-a-scene
  sceneSetup = () => {
    // get container dimensions and use them for scene sizing
    const width = this.el.clientWidth;
    const height = this.el.clientHeight;

    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(
      75, // fov = field of view
      width / height, // aspect ratio
      0.1, // near plane
      1000 // far plane
    );
    // this.camera.position.z = 5; // is used here to set some distance from a cube that is located at z = 0
    // OrbitControls allow a camera to orbit around the object
    // https://threejs.org/docs/#examples/controls/OrbitControls
    
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(width, height);
    this.renderer.setClearColor(0x000000, 1);    
    this.controls = new OrbitControls(this.camera, this.el);
    this.el.appendChild(this.renderer.domElement); // mount using React ref
  };

  // Code below is taken from Three.js BoxGeometry example
  // https://threejs.org/docs/#api/en/geometries/BoxGeometry
  addCustomSceneObjects = () => {
    const loader = new STLLoader();
    loader.load(
      stlUrl,
      (bufferGeometry) => {
        if (this.scene.children) {

          for(let i =0;i<this.scene.children.length-1;i++){
            this.scene.remove(this.scene.children[i]);
          }
          
        }
        const geometry = new THREE.Geometry().fromBufferGeometry(
          bufferGeometry
        );
        this.addUV(geometry);

        const material = new THREE.MeshStandardMaterial({
          color: 0xeeeeee,
          roughness: 0.01,
        });
        this.mesh = new THREE.Mesh(geometry, material);

        this.scene.add(this.mesh);
        let cc = new THREE.Box3().setFromObject(this.mesh).getCenter();
        this.controls.reset();
        this.controls.target.set(cc.x, cc.y, cc.z);

        this.cameraDistance = Math.max(
          geometry.boundingBox.max.x * 3,
          geometry.boundingBox.max.y * 3,
          geometry.boundingBox.max.z * 3
        );
        this.camera.position.set(0, 0, this.cameraDistance);
        this.csize_x = this.size_x =
          geometry.boundingBox.max.x - geometry.boundingBox.min.x;
        this.csize_y = this.size_y =
          geometry.boundingBox.max.y - geometry.boundingBox.min.y;
        this.csize_z = this.size_z =
          geometry.boundingBox.max.z - geometry.boundingBox.min.z;

        this.displayUnits(this.size_x, this.size_y, this.size_z);

        var a = document.getElementById("in-dim-percent");
        if (a) a.value = 100;

        var b = document.getElementById("op-dim-percent");
        if (b) b.innerText = 100 + "%";
        this.startAnimationLoop();
      },
      (progressEvent) => {
        if (!progressEvent.lengthComputable) {
          this.setState({ showLoading: false });
        }
      }
    );

    this.addLights();
  };

  addLights=()=>{
    const lights = [];
    lights[0] = new THREE.PointLight(0xffffff, 1, 0);
    lights[1] = new THREE.PointLight(0xffffff, 1, 0);
    lights[2] = new THREE.PointLight(0xffffff, 1, 0);

    lights[0].position.set(0, 200, 0);
    lights[1].position.set(100, 200, 100);
    lights[2].position.set(-100, -200, -100);

    this.scene.add(lights[0]);
    this.scene.add(lights[1]);
    this.scene.add(lights[2]);

    let alight = new THREE.AmbientLight(0xffffff);
    let dlight = new THREE.DirectionalLight(0xffffff, 0.5);
    this.scene.add(dlight);
    this.scene.add(alight);
  }
  addUV = (geometry) => {
    geometry.computeBoundingBox();

    var max = geometry.boundingBox.max,
      min = geometry.boundingBox.min;
    var offset = new THREE.Vector2(0 - min.x, 0 - min.y);
    var range = new THREE.Vector2(max.x - min.x, max.y - min.y);
    var faces = geometry.faces;
    geometry.faceVertexUvs[0] = [];

    for (var i = 0; i < faces.length; i++) {
      var v1 = geometry.vertices[faces[i].a],
        v2 = geometry.vertices[faces[i].b],
        v3 = geometry.vertices[faces[i].c];

      geometry.faceVertexUvs[0].push([
        new THREE.Vector2(
          (v1.x + offset.x) / range.x,
          (v1.y + offset.y) / range.y
        ),
        new THREE.Vector2(
          (v2.x + offset.x) / range.x,
          (v2.y + offset.y) / range.y
        ),
        new THREE.Vector2(
          (v3.x + offset.x) / range.x,
          (v3.y + offset.y) / range.y
        ),
      ]);
    }

    geometry.uvsNeedUpdate = true;
  };

  numberWithCommas = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };

  displayUnits = (xsize, ysize, zsize) => {
    const row = document.getElementById("model-dimension");
    if (row.children[1])
      row.children[1].innerText = this.numberWithCommas(xsize.toFixed(2));
    if (row.children[3])
      row.children[3].innerText = this.numberWithCommas(ysize.toFixed(2));
    if (row.children[5])
      row.children[5].innerText = this.numberWithCommas(zsize.toFixed(2));
  };

  startAnimationLoop = () => {
    this.renderer.render(this.scene, this.camera);

    // The window.requestAnimationFrame() method tells the browser that you wish to perform
    // an animation and requests that the browser call a specified function
    // to update an animation before the next repaint
    this.requestID = window.requestAnimationFrame(this.startAnimationLoop);
  };

  handleWindowResize = () => {
    const width = this.el.clientWidth;
    const height = this.el.clientHeight;

    this.renderer.setSize(width, height);
    this.camera.aspect = width / height;

    // Note that after making changes to most of camera properties you have to call
    // .updateProjectionMatrix for the changes to take effect.
    this.camera.updateProjectionMatrix();
  };

  setOrientation = (value) => {
    this.mesh = this.scene.children[1];
    let cc = new THREE.Box3().setFromObject(this.mesh).getCenter();
    this.controls.reset();
    this.controls.target.set(cc.x, cc.y, cc.z);
    switch (value) {
      case "front":
        this.camera.position.set(0, this.cameraDistance, 0);
        break;
      case "top":
        this.camera.position.set(0, 0, this.cameraDistance);
        break;
      default:
    }
  };

  changeUnit(value) {
    let percent = 100;
    let a = document.getElementById("in-dim-percent");
    if (a) percent = a.value;

    this.recalDimension(value, percent);
  }

  recalDimension = (unit, percent) => {
    if (unit === "in") {
      this.displayUnits(
        (this.size_x / 25.4) * (percent / 100),
        (this.size_y / 25.4) * (percent / 100),
        (this.size_z / 25.4) * (percent / 100)
      );
    } else {
      this.displayUnits(
        this.size_x * (percent / 100),
        this.size_y * (percent / 100),
        this.size_z * (percent / 100)
      );
    }
  };

  applyTexture = (e) => {
    let texturePath = this.getTexturePath(e.target.value);

    const texUrl =
      process.env.PUBLIC_URL + "./../assets/textures/" + texturePath;
    this.changeSceneMaterial(texUrl);
  };

  getTexture = () => {
    let texPath = this.getTexturePath();
    let texMap = null;

    if (texPath !== "") {
      let texLoader = new THREE.TextureLoader();
      texMap = texLoader.load(texPath);
    }

    return texMap;
  };

  changeSceneMaterial = (texUrl) => {
    new THREE.TextureLoader().load(
      texUrl,
      (texture) => {
        this.mesh.material.map = texture;
        this.mesh.material.needsUpdate = true;
      },
      (xhr) => {
        //Download Progress
        console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
      },
      (error) => {
        //Error CallBack
        console.log("An error happened", error);
      }
    );
  };

  getTexturePath = (selectedMaterial) => {
    switch (selectedMaterial) {
      case "m1":
        return "UV_Grid_Sm.jpg";

      case "m2":
        return "ceramic.jpeg";

      case "m3":
        return "copper1.jpeg";

      default:
        return "default.png";
    }
  };

  changeDimension = (e) => {
    let unit = "mm";
    let u = document.getElementById("unit-select");
    if (u) unit = u.value;
    this.recalDimension(unit, e.target.value);

    const a = document.getElementById("op-dim-percent");
    if (a) a.innerText = e.target.value + "%";
  };

  render() {
    return <div style={style} ref={(ref) => (this.el = ref)} />;
  }
}

export default StlViewer;
