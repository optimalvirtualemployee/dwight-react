import React, { Component } from "react";

import AppContext from "contexts/AppContext";

class LanguageSwitcher extends Component {
  render() {
    return (
      <AppContext.Consumer>
        {({ language, setLanguage }) => (
          <button onClick={() => setLanguage("jp")}>
            Switch Language (Current: {language})
          </button>
        )}
      </AppContext.Consumer>
    );
  }
}

export default LanguageSwitcher;
