import React, { Component } from "react";
import "./Promotions.css";

import { connect } from "react-redux";
import { homeActions, shopActions } from "store/actions";
//import { NavLink } from "react-router-dom";
import { Link } from 'react-router-dom';
import config from "services/config";
//import { history } from "store/helpers";

class Promotions extends Component {
  constructor(props) {
    super(props);
    this.props.dispatch(homeActions.getBanners())
  }

  renderPromotionsSection() {
    if (this.props.promotions) {
      if (this.props.promotions.items) {
        return this.props.promotions.items.map((promotion, index) => {
          return (
            <React.Fragment key={index}>
              <div className="col-lg-4 col-md-6 img-one">
                <img
                  src={promotion.image}
                  alt=" "
                  className="img-fluid"
                />
              </div>
              <div
                className={`col-lg-4 col-md-6 ${index === 0 ? 'img-one' : 'img-info'} content-mid py-md-0 py-5`}
              >
                <div className="info">
                  <h3>{promotion.title}</h3>
                  <p className="mt-3 mb-5 white text-truncate" dangerouslySetInnerHTML={{ __html: promotion.short_description }} />
                  {promotion.link !== "services" ? promotion.link !== "contact" ?
                    <Link to={`/page/${promotion.link}`} className={`btn ${index === 0 ? 'btn-light' : 'btn-primary'} theme-button`}>
                      {promotion.title}
                    </Link> : <Link to='contact' className={`btn ${index === 0 ? 'btn-light' : 'btn-primary'} theme-button`}>
                      {promotion.title}
                    </Link>:
                    <Link to={`/category/${config.CATEGORY_DESIGN_PRODUCT}`} className={`btn ${index === 0 ? 'btn-light' : 'btn-primary'} theme-button`}>
                      {promotion.title}
                    </Link>
                  }

                </div>
              </div>
            </React.Fragment>
          );
        });
      }
    }

  }

  render() {
    return (
      <section className="w3l-index2">
        <div className="row abouthy-img-grids no-gutters">
          {this.renderPromotionsSection()}
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Promotions);
