import React from "react";

// set the defaults
const AppContext = React.createContext({
    cartItems: JSON.parse(localStorage.getItem('cartItems')) != null ? JSON.parse(localStorage.getItem('cartItems')) : [],
    setCartItems: () => {},
});

export default AppContext;
