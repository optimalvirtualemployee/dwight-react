const config = {
    apiUrl: 'http://django.optimaldevelopments.com',
    //apiUrl: 'http://localhost:8000',
    CATEGORY_ENTERPRISE_PRODUCT: 'EP',
    CATEGORY_NON_ENTERPRISE_PRODUCT: 'CP',
    CATEGORY_DESIGN_PRODUCT: 'DS',
    ENTERPRISE_PRODUCT_TEXT: 'Enterprise Products',
    NON_ENTERPRISE_PRODUCT_TEXT: 'Consumer Products',
    DESIGN_PRODUCT_TEXT: 'Design Services',
    CONTACT_US_SLUG: 'contact-us',
    ABOUT_US_SLUG: 'about-us',
    CONTACT_PAGE_URL: '/contact',
    FOOTER_TYPE_QUICK_LINKS: 'quick_links',
    FOOTER_TYPE_EXPLORE_LINKS: 'explore_links',
    img_url: 'http://django.optimaldevelopments.com/media/',
    cartItem: [
        {
            items: [],
            subTotal: null,
            total: null
        }
    ],
    PAYPAL: {
        RETURN_URL: 'http://localhost/thankyou',
        CANCEL_URL: 'http://localhost/thankyou'
    },
}

export default config;
